## basic package
import sys
import re
import fnmatch
from copy import deepcopy
from collections import defaultdict

import mindspore.dataset.transforms as transforms
import mindspore.dataset.vision as vision

## self-defined package
from .auto_aug import CIFAR10Policy, Cutout


__all__ = ['list_transforms', 'is_transforms', 'transforms_entry', 'list_modules', 'is_transforms_in_modules',
        'is_transforms_default_key', 'has_transforms_default_key', 'get_transforms_default_value',]

_transforms_entry = dict()
_module_to_transforms = defaultdict(set)
_transforms_to_module = dict()
_transforms_default_cfgs = dict()

def register_transforms(fn):

    mod = sys.modules[fn.__module__]
    module_name_split = fn.__module__.split('.')
    module_name = module_name_split[-1] if module_name_split else ''
    print(module_name_split, module_name)

    #add transforms to __all__ in module
    transforms_name = fn.__name__
    if hasattr(mod, '__all__'):
        mod.__all__.append(transforms_name)
    else:
        mod.__all__ = [transforms_name]

    #add entries to registry dict
    _transforms_entry[transforms_name] = fn
    _transforms_to_module[transforms_name] = module_name
    _module_to_transforms[module_name].add(transforms_name)

    #
    if hasattr(mod, 'default_cfgs') and transforms_name in mod.default_cfgs:
        _transforms_default_cfgs[transforms_name] = deepcopy(mod.default_cfgs[transforms_name])

    return fn

def _natural_key(string_):
    return [int(s) if s.isdigit() else s for s in re.split(r'(\d+)', string_.lower())]

def list_transforms(filters='', module='', exclude_filters='', name_matches_cfg=False):
    if module:
        all_transforms = list(_module_to_transforms[module])
    else:
        all_transforms = _transforms_entry.keys()

    if filters:
        transforms = []
        include_filters = filters if isinstance(filters, (tuple, list)) else [filters]
        for f in include_filters:
            include_transforms = fnmatch.filter(all_transforms, f)  # include these models
            if include_transforms:
                transforms = set(transforms).union(include_transforms)
    else:
        transforms = all_transforms

    if exclude_filters:
        if not isinstance(exclude_filters, (tuple, list)):
            exclude_filters = [exclude_filters]
        for xf in exclude_filters:
            exclude_transforms = fnmatch.filter(transforms, xf)  # exclude these models
            if exclude_transforms:
                transforms = set(transforms).difference(exclude_transforms)
    if name_matches_cfg:
        transforms = set(_transforms_default_cfgs).intersection(transforms)
    return list(sorted(transforms, key=_natural_key))

def is_transforms(transforms_name):
    return transforms_name in _transforms_entry

def transforms_entry(transforms_name):
    return _transforms_entry[transforms_name]

def list_modules():
    modules = _module_to_transforms.keys()
    return list(sorted(modules))


def is_transforms_in_modules(transforms_name, module_names):
    assert isinstance(module_names, (tuple, list, set))
    return any(transforms_name in _module_to_transforms[n] for n in module_names)


def has_transforms_default_key(transforms_name, cfg_key):
    """ Query transforms default_cfgs for existence of a specific key.
    """
    if transforms_name in _transforms_default_cfgs and cfg_key in _transforms_default_cfgs[transforms_name]:
        return True
    return False


def is_transforms_default_key(transforms_name, cfg_key):
    """ Return truthy value for specified transforms default_cfg key, False if does not exist.
    """
    if transforms_name in _transforms_default_cfgs and _transforms_default_cfgs[transforms_name].get(cfg_key, False):
        return True
    return False


def get_transforms_default_value(transforms_name, cfg_key):
    """ Get a specific transforms default_cfg value by key. None if it doesn't exist.
    """
    if transforms_name in _transforms_default_cfgs:
        return _transforms_default_cfgs[transforms_name].get(cfg_key, None)
    return None

########################################################################################################################

@register_transforms
def cifar_weak_train(args):
    return transforms.Compose([
        vision.RandomCrop(args.crop_size, padding=args.pad_size, fill_value=args.fill_value),
        vision.RandomHorizontalFlip(),
        vision.Normalize(args.img_mean, args.img_std),
        vision.ToTensor(),
    ])


@register_transforms
def cifar_strong_train(args):

    return transforms.Compose([
        vision.RandomCrop(args.crop_size, padding=args.pad_size, fill_value=args.fill_value),
        vision.RandomHorizontalFlip(),
        CIFAR10Policy(),
        vision.ToTensor(),
        Cutout(n_holes=args.cutout_num, length=args.cutout_len),
        vision.Normalize(args.img_mean, args.img_std),
    ])

@register_transforms
def cifar_val(args):
    return transforms.Compose([
        vision.Normalize(args.img_mean, args.img_std),
        vision.ToTensor(),
        ])

@register_transforms
def imagenet_weak_train(args):
    return None


@register_transforms
def imagenet_strong_train(args):
    return None

@register_transforms
def imagenet_val(args):
    return None

@register_transforms
def place_weak_train(args):
    return None

@register_transforms
def place_strong_train(args):
    return None

@register_transforms
def place_val(args):
    return None

@register_transforms
def inaturalist_weak_train(args):
    return None

@register_transforms
def inaturalist_strong_train(args):
    return None

@register_transforms
def inaturalist_val(args):
    return None
