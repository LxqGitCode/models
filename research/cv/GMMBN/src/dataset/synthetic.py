import os
import mindspore
from mindspore.communication import init, get_rank, get_group_size

from .data_registry import register_dataset

mindspore.set_context(mode=mindspore.PYNATIVE_MODE, device_target='Ascend')
init(backend_name="hccl")
def clc_path_join(*args):
    out = ""
    for arg in args:
        out = os.path.join(out, arg)
    return out
def clc_cat_join(*args):
    return "".join(args)

@register_dataset
class cifar10(mindspore.dataset.Cifar10Dataset):
    cls_num = 10
    def __init__(self,
            dataset_dir="/data/",
            usage='train',
            num_parallel_workers=8,
            shuffle=False,
            num_shards=get_group_size(),
            shard_id=get_rank()
           ):
        # print(num_shards, shard_id, "******")
        super(cifar10, self).__init__(dataset_dir, usage, \
                                     num_parallel_workers=num_parallel_workers, \
                                        shuffle=shuffle, num_shards=num_shards, shard_id=shard_id)
        self.root = dataset_dir
        self.train = usage == 'train'

    def get_img_num_per_cls(self, cls_num, imb_factor):
        data_len = sum(1 for _ in self.create_tuple_iterator())
        img_max = data_len / cls_num
        img_num_per_cls = []
        for cls_idx in range(cls_num):
            num = img_max * (imb_factor ** (cls_idx / (cls_num - 1.0)))
            img_num_per_cls.append(int(num))

        return img_num_per_cls

    def __len__(self):
        return len(self.targets)
