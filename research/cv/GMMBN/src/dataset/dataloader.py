import mindspore
from mindspore.dataset.transforms import TypeCast
from mindspore.communication import init, get_rank, get_group_size

from .data_registry import dataset_entry
from .transforms  import transforms_entry


def data_loader(args):
    mindspore.set_context(mode=mindspore.PYNATIVE_MODE, device_target='Ascend')
    init()
    rank_id = get_rank()
    rank_size = get_group_size()
    if 'cifar' in args.data_name:
        weak_trans = transforms_entry('cifar_weak_train')(args)
        strong_trans = transforms_entry('cifar_strong_train')(args)
        val_trans = transforms_entry('cifar_val')(args)

        train_dataset = dataset_entry(args.data_name)(args.data_folder, 'train', num_parallel_workers=args.workers, \
                                                      shuffle=True, num_shards=rank_size, shard_id=rank_id)
        args.cls_num_list = train_dataset.get_img_num_per_cls(train_dataset.cls_num, imb_factor=0.01)
        train_dataset_ = dataset_entry(args.data_name)(args.data_folder, 'train', \
                                                       num_parallel_workers=args.workers, shuffle=True)
        args.cls_num_list = train_dataset.get_img_num_per_cls(train_dataset.cls_num, imb_factor=0.01)

        train_dataset_weak = train_dataset.map(operations=weak_trans, input_columns='image')
        train_dataset_strong = train_dataset_.map(operations=strong_trans, input_columns='image')

        val_dataset = dataset_entry(args.data_name)(args.data_folder, 'test', num_parallel_workers=args.workers)
        val_dataset = val_dataset.map(operations=val_trans, input_columns='image')

        #注意只有使用main_.py中Model高级API才需要这样
        type_cast_op = TypeCast(mindspore.int32)
        train_dataset_weak = train_dataset_weak.map(operations=type_cast_op, input_columns='label')
        val_dataset = val_dataset.map(operations=type_cast_op, input_columns='label')
    else:
        weak_trans = transforms_entry(args.data_name+'_weak_train')(args)
        strong_trans = transforms_entry(args.data_name+'_strong_train')(args)
        val_trans = transforms_entry(args.data_name+'_val')(args)

        train_dataset = dataset_entry(args.data_name)(root=args.data_folder, train=True, imb_factor=args.data_ratio,\
                random_seed=args.data_seed, download=True, transform=weak_trans, target_transform=strong_trans)

        val_dataset = dataset_entry(args.data_name)(root=args.data_folder, train=False, imb_factor=args.data_ratio, \
                random_seed=args.data_seed, download=True, transform=val_trans)

    train_dataset_weak = train_dataset_weak.batch(batch_size=args.batch_size, drop_remainder=True)
    train_dataset_strong = train_dataset_strong.batch(batch_size=args.batch_size, drop_remainder=True)
    val_dataset = val_dataset.batch(batch_size=args.batch_size, drop_remainder=True)

    return args, train_dataset_weak, train_dataset_strong, val_dataset
