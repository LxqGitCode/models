# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""Evaluation for GuidedAnchoring."""
import argparse
import json
import os
import pathlib
import time
from pprint import pprint

import numpy as np
import mindspore as ms
from mindspore.common import set_seed
from pycocotools.coco import COCO
from pycocotools.cocoeval import COCOeval
from tqdm import tqdm

from src.config import (
    Config,
    compute_features_info,
    merge,
    parse_cli_to_yaml,
    parse_yaml,
)
from src.dataset import create_mindrecord_dataset
from src.guided_anchoring.bbox.multiclass_nms import multiclass_nms
from src.guided_anchoring.detectors import create_model


def get_config():
    """Get Config according to the yaml file and cli arguments."""
    parser = argparse.ArgumentParser(description='default name',
                                     add_help=False)
    parser.add_argument('--config_path', type=pathlib.Path,
                        required=True,
                        help='Config file path')
    parser.add_argument('--checkpoint',
                        help='Path to save checkpoint.')
    parser.add_argument('--eval_results_path',
                        default='eval_results',
                        help='Path to folder with evaluation results.')
    parser.add_argument('--prediction_path',
                        help='Path to model predictions.')
    path_args, _ = parser.parse_known_args()
    default, helper, choices = parse_yaml(path_args.config_path)
    args = parse_cli_to_yaml(parser=parser, cfg=default, helper=helper,
                             choices=choices, cfg_path=path_args.config_path)
    final_config = Config(merge(args, default))
    final_config = compute_features_info(final_config)
    pprint(final_config)
    print('Please check the above information for the configurations',
          flush=True)
    return final_config


def rcnn_eval(ds, ckpt_path, anno_path):
    """RCNN model evaluation."""
    if not os.path.isfile(ckpt_path):
        raise RuntimeError(f'CheckPoint file {ckpt_path} is not valid.')
    img_ids = []
    predictions = []
    classs_dict = {}
    val_cls = config.coco_classes
    val_cls_dict = {}
    for i, cls in enumerate(val_cls):
        val_cls_dict[i] = cls
    coco_gt = COCO(anno_path)
    cat_ids = coco_gt.getCatIds()
    cats = coco_gt.loadCats(cat_ids)
    for cat in cats:
        classs_dict[cat['name']] = cat['id']
    classs_dict['background'] = 0
    print(classs_dict)

    net = create_model(config.model)
    param_dict = ms.load_checkpoint(ckpt_path)
    ms.load_param_into_net(net, param_dict)
    net.set_train(False)
    eval_iter = 0
    total = ds.get_dataset_size()

    print('\n========================================\n')
    print('total images num: ', total)
    print('Processing, please wait a moment.')
    for _, data in enumerate(tqdm(
            ds.create_dict_iterator(num_epochs=1),
            total=ds.get_dataset_size()
    )):
        eval_iter = eval_iter + 1
        img_data = data['image']
        img_metas = data['image_shape']

        img_id = np.reshape(
            data['image_shape'][:, -1:], (-1,)
        ).astype(int)
        img_ids.append(img_id)

        # run net
        output = net(img_data, img_metas)

        bboxes, labels = output[0]
        bboxes, labels = bboxes.asnumpy(), labels.asnumpy()

        bboxes, scores, labels = multiclass_nms(
            bboxes, labels,
            num_classes=config.num_classes
        )
        metas = img_metas[0].asnumpy()
        scale = np.array([metas[3], metas[2],
                          metas[3], metas[2]])

        for i in range(len(bboxes)):
            loc = bboxes[i].astype(float)
            if config.model.test_cfg.get('rcnn') is not None:
                loc = loc.reshape((-1, 4))
            loc = loc / scale
            label = int(labels[i])
            if label > 80:
                continue
            if config.model.test_cfg.get('rcnn') is not None:
                loc = loc[label - 1]
            score = float(scores[i])
            res = {
                'image_id': int(img_id),
                'bbox': [loc[0], loc[1],
                         loc[2] - loc[0],
                         loc[3] - loc[1]],
                'score': score,
                'category_id': cat_ids[label - 1]
            }
            predictions.append(res)

    # Evaluate with received predictions in JSON
    with open('predictions.json', 'w') as f:
        json.dump(predictions, f)

    coco_dt = coco_gt.loadRes('predictions.json')
    cat_ids = coco_gt.getCatIds(catNms=config.coco_classes)
    img_ids = coco_gt.getImgIds()
    e = COCOeval(coco_gt, coco_dt, iouType='bbox')
    e.params.imgIds = img_ids
    e.params.catIds = cat_ids
    e.evaluate()
    e.accumulate()
    e.summarize()
    m_ap = e.stats[0]
    print('\n========================================\n')
    print(f'mAP: {m_ap}')


def main():
    """Eval GuidedAnchoring."""
    val_dataset = create_mindrecord_dataset(
        path=config.val_dataset, config=config, training=False,
        python_multiprocessing=config.python_multiprocessing
    )
    print('CHECKING MINDRECORD FILES DONE!')
    print('Start Eval!')
    start_time = time.time()
    rcnn_eval(
        val_dataset, config.checkpoint,
        os.path.join(config.val_dataset, 'labels.json')
    )
    end_time = time.time()
    total_time = end_time - start_time
    print(f'\nDone!\nTime taken: {int(total_time)} seconds')


if __name__ == '__main__':
    set_seed(1)
    config = get_config()
    if hasattr(config, 'pynative_mode') and config.pynative_mode is False:
        mode = ms.GRAPH_MODE
    else:
        mode = ms.PYNATIVE_MODE
    print('Mode:', 'GRAPH' if mode is ms.GRAPH_MODE else 'PYNATIVE')
    ms.set_context(
        mode=mode, device_target=config.device_target,
        max_call_depth=2000
    )
    config.rank = 0
    main()
