# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
from typing import Optional, Union

from ...config import Config
from .assigners import ApproxMaxIoUAssigner, MaxIoUAssigner
from .delta_xywh_bbox_coder import DeltaXYWHBBoxCoder
from .samplers import PseudoSampler, RandomSampler


def create_coder(config: Config) -> DeltaXYWHBBoxCoder:
    coder = DeltaXYWHBBoxCoder(
        target_means=config.target_means,
        target_stds=config.target_stds
    )
    return coder


def create_assigner(
        config: Config
) -> Union[ApproxMaxIoUAssigner, MaxIoUAssigner]:
    if config.type == 'MaxIoUAssigner':
        cls = MaxIoUAssigner
    elif config.type == 'ApproxMaxIoUAssigner':
        cls = ApproxMaxIoUAssigner
    else:
        raise RuntimeError(f'Unknown assigner type: {config.type}')
    return cls(
        pos_iou_thr=config.pos_iou_thr,
        neg_iou_thr=config.neg_iou_thr,
        min_pos_iou=config.get('min_pos_iou', 0.0),
        ignore_iof_thr=config.get('ignore_iof_thr', -1),
        match_low_quality=config.get('match_low_quality', True),
    )


def create_sampler(
        config: Optional[Config]
) -> Union[RandomSampler, PseudoSampler]:
    if config is not None and config.type == 'RandomSampler':
        return RandomSampler(
            num=config.num,
            pos_fraction=config.pos_fraction,
            neg_pos_ub=config.neg_pos_ub,
            add_gt_as_proposals=config.add_gt_as_proposals,
        )
    if config is None:
        return PseudoSampler()
    raise RuntimeError(f'Unknown sampler type: {config.type}')
