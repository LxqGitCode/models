# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
#
# This file has been derived from the
# https://github.com/open-mmlab/mmdetection/tree/v2.28.2
# repository and modified.
# ============================================================================
import warnings
from typing import Optional

import mindspore as ms
import mindspore.nn as nn
import mindspore.numpy as np
import mindspore.ops as ops

from ..anchor import AnchorGenerator
from ..anchor.utils import (
    anchor_inside_flags,
    calc_region,
    images_to_levels,
)
from ..bbox import (
    ApproxMaxIoUAssigner,
    DeltaXYWHBBoxCoder,
)
from ..losses import (
    BoundedIoULoss,
    SigmoidFocalClassificationLoss,
)
from ..misc import unmap, multi_apply
from ..ops import DeformConv2d, MaskedConv2d


class FeatureAdaption(nn.Cell):
    """
    Feature Adaption Module.

    Feature Adaption Module is implemented based on DCN v1.
    It uses anchor shape prediction rather than feature map to
    predict offsets of deform conv layer.

    Args:
    ----
        in_channels (int): Number of channels in the input feature map.
        out_channels (int): Number of channels in the output feature map.
        kernel_size (int): Deformable conv kernel size.
        deform_groups (int): Deformable conv group size.
        init_cfg (dict or list[dict], optional): Initialization config dict.
    """

    def __init__(self,
                 in_channels,
                 out_channels,
                 kernel_size=3,
                 deform_groups=4):
        super().__init__()
        offset_channels = kernel_size * kernel_size * 2
        self.conv_offset = nn.Conv2d(
            2, deform_groups * offset_channels, 1,
            has_bias=False)
        self.conv_adaption = DeformConv2d(
            in_channels,
            out_channels,
            kernel_size=kernel_size,
            padding=(kernel_size - 1) // 2,
            deform_groups=deform_groups)
        self.relu = nn.ReLU()

    def construct(self, x, shape):
        offset = self.conv_offset(shape)
        x = self.relu(self.conv_adaption(x, offset))
        return x


class GuidedAnchorHead(nn.Cell):
    """
    Guided-Anchor-based head (GA-RPN, GA-RetinaNet, etc.).

    This GuidedAnchorHead will predict high-quality feature guided
    anchors and locations where anchors will be kept in inference.
    There are mainly 3 categories of bounding-boxes.

    - Sampled 9 pairs for target assignment. (approxes)
    - The square boxes where the predicted anchors are based on. (squares)
    - Guided anchors.
    """

    def __init__(
            self,
            num_classes: int,
            in_channels: int,
            approx_anchor_generator: AnchorGenerator,
            square_anchor_generator: AnchorGenerator,
            anchor_coder: DeltaXYWHBBoxCoder,
            bbox_coder: DeltaXYWHBBoxCoder,
            loss_loc: SigmoidFocalClassificationLoss,
            loss_bbox: nn.SmoothL1Loss,
            loss_cls: SigmoidFocalClassificationLoss,
            loss_shape: BoundedIoULoss,
            use_sigmoid_cls,
            loc_focal_loss: bool,
            sampling: bool,
            test_cfg,
            train_cfg=None,
            feat_channels: int = 256,
            reg_decoded_bbox=False,
            deform_groups=4,
            loc_filter_thr=0.01,
            ga_assigner: Optional[ApproxMaxIoUAssigner] = None,
            assigner=None,
            sampler=None,
            ga_sampler=None,
            ga_sampling=False,
    ):
        super().__init__()

        # number of upstream feature maps
        self.num_layers = 5

        self.in_channels = in_channels
        self.num_classes = num_classes
        self.feat_channels = feat_channels
        self.deform_groups = deform_groups
        self.loc_filter_thr = loc_filter_thr

        self.approx_anchor_generator = approx_anchor_generator
        self.square_anchor_generator = square_anchor_generator
        self.approxs_per_octave = self.approx_anchor_generator \
            .num_base_priors[0]

        self.reg_decoded_bbox = reg_decoded_bbox

        # one anchor per location
        self.num_base_priors = self.square_anchor_generator.num_base_priors[0]

        self.use_sigmoid_cls = use_sigmoid_cls
        self.loc_focal_loss = loc_focal_loss
        self.sampling = sampling
        self.ga_sampling = ga_sampling

        if self.use_sigmoid_cls:
            self.cls_out_channels = self.num_classes
        else:
            self.cls_out_channels = self.num_classes + 1

        self.anchor_coder = anchor_coder
        self.bbox_coder = bbox_coder

        self.sum_loss = ops.ReduceSum()
        self.loss_bbox = loss_bbox
        self.loss_cls = loss_cls
        self.loss_loc = loss_loc
        self.loss_shape = loss_shape

        self.train_cfg = train_cfg
        self.test_cfg = test_cfg

        self.nms_pre = test_cfg['nms_pre']
        self.max_per_img = test_cfg['max_per_img']
        self.min_bbox_size = test_cfg['min_bbox_size']
        self.iou_threshold = self.test_cfg['nms'].get('iou_threshold', 0.5)

        self.wh_ratio_clip = ms.ops.Tensor(1e-6)

        self.sampler = sampler
        self.ga_sampler = ga_sampler
        self.assigner = assigner
        self.ga_assigner = ga_assigner

        self._init_layers()

        self.permute = ops.Transpose()

    @property
    def num_anchors(self):
        warnings.warn('DeprecationWarning: `num_anchors` is deprecated, '
                      'please use "num_base_priors" instead')
        return self.square_anchor_generator.num_base_priors[0]

    def _init_layers(self):
        self.relu = nn.ReLU()
        self.conv_loc = nn.Conv2d(self.in_channels, 1, 1,
                                  has_bias=True)
        self.conv_shape = nn.Conv2d(self.in_channels, self.num_base_priors * 2,
                                    1, has_bias=True)
        self.feature_adaption = FeatureAdaption(
            self.in_channels,
            self.feat_channels,
            kernel_size=3,
            deform_groups=self.deform_groups)
        self.conv_cls = MaskedConv2d(
            self.feat_channels, self.num_base_priors * self.cls_out_channels,
            1)
        self.conv_reg = MaskedConv2d(self.feat_channels,
                                     self.num_base_priors * 4, 1)

    def forward_single(self, x):
        loc_pred = self.conv_loc(x)
        shape_pred = self.conv_shape(x)
        x = self.feature_adaption(x, shape_pred)

        # masked conv is only used during inference for speed-up
        if not self.training:
            mask = ops.sigmoid(loc_pred) >= self.loc_filter_thr
            mask = mask[:, 0, :, :]
        else:
            mask = None
        cls_score = self.conv_cls(x, mask)
        bbox_pred = self.conv_reg(x, mask)
        return cls_score, bbox_pred, shape_pred, loc_pred

    def construct(self, feats):
        res0 = self.forward_single(feats[0][0])
        res1 = self.forward_single(feats[0][1])
        res2 = self.forward_single(feats[0][2])
        res3 = self.forward_single(feats[0][3])
        res4 = self.forward_single(feats[0][4])
        return tuple(map(list, zip(res0, res1, res2, res3, res4)))

    def get_sampled_approxs(self, featmap_sizes, img_metas):
        """
        Get sampled approxs and inside flags according to feature map sizes.

        Args:
        ----
            featmap_sizes (list[tuple]): Multi-level feature map sizes.
            img_metas: Image meta info.

        Returns:
        -------
            tuple: approxes of each image, inside flags of each image
        """
        num_imgs = len(img_metas)

        # since feature map sizes of all images are the same, we only compute
        # approxes for one time
        multi_level_approxs = self.approx_anchor_generator.grid_priors(
            featmap_sizes)
        approxs_list = [multi_level_approxs for _ in range(num_imgs)]

        # for each image, we compute inside flags of multi level approxes
        inside_flag_list = []
        for img_id, img_meta in enumerate(img_metas):
            multi_level_flags = []
            multi_level_approxs = approxs_list[img_id]

            # obtain valid flags for each approx first
            # TODO: What is [800, 1344]? Maybe fix to correct shape
            multilevel_approx_flags = self.approx_anchor_generator.valid_flags(
                featmap_sizes, [800, 1344])

            for idx, flags in enumerate(multilevel_approx_flags):
                approxs = multi_level_approxs[idx]
                inside_flags_list = []
                for i in range(self.approxs_per_octave):
                    split_valid_flags = flags[i::self.approxs_per_octave]
                    split_approxs = approxs[i::self.approxs_per_octave, :]
                    inside_flags = anchor_inside_flags(
                        split_approxs, split_valid_flags,
                        img_meta[:2],
                        self.train_cfg['allowed_border'])
                    inside_flags_list.append(inside_flags)
                # inside_flag for a position is true if any anchor in this
                # position is true
                inside_flags = (
                    ops.stack(inside_flags_list, 0).sum(axis=0) > 0)
                multi_level_flags.append(inside_flags)
            inside_flag_list.append(multi_level_flags)
        return approxs_list, inside_flag_list

    def _get_guided_anchors_single(self,
                                   squares,
                                   shape_pred,
                                   loc_pred,
                                   use_loc_filter=False):
        """
        Get guided anchors and loc masks for a single level.

        Args:
        ----
            square (tensor): Squares of a single level.
            shape_pred (tensor): Shape predictions of a single level.
            loc_pred (tensor): Loc predictions of a single level.
            use_loc_filter (bool): Use loc filter or not.

        Returns:
        -------
            tuple: guided anchors, location masks
        """
        # calculate location filtering mask
        loc_pred = ops.sigmoid(loc_pred)

        if use_loc_filter:
            loc_mask = loc_pred >= self.loc_filter_thr
        else:
            loc_mask = loc_pred >= 0.0
        mask = self.permute(loc_mask, (1, 2, 0))
        mask = ops.broadcast_to(mask, (mask.shape[0], mask.shape[1],
                                       self.num_base_priors))
        mask = mask.view(-1)

        # calculate guided anchors
        anchor_deltas = self.permute(shape_pred, (1, 2, 0)).view(-1, 2)
        bbox_deltas = np.full(squares.shape, 0).astype(ms.float32)
        bbox_deltas[:, 2:] = anchor_deltas

        guided_anchors = self.anchor_coder.decode(
            squares, bbox_deltas, wh_ratio_clip=self.wh_ratio_clip)
        return guided_anchors, mask

    def get_anchors(self,
                    featmap_sizes,
                    shape_preds,
                    loc_preds,
                    img_metas,
                    use_loc_filter=False):
        """
        Get squares according to feature map sizes and guided anchors.

        Args:
        ----
            featmap_sizes (list[tuple]): Multi-level feature map sizes.
            shape_preds (list[tensor]): Multi-level shape predictions.
            loc_preds (list[tensor]): Multi-level location predictions.
            img_metas: Image meta info.
            use_loc_filter (bool): Use loc filter or not.

        Returns:
        -------
            tuple: square approxs of each image, guided anchors of each image,
                loc masks of each image
        """
        num_imgs = len(img_metas)

        # since feature map sizes of all images are the same, we only compute
        # squares for one time
        multi_level_squares = self.square_anchor_generator.grid_priors(
            featmap_sizes)
        squares_list = [multi_level_squares for _ in range(num_imgs)]

        # for each image, we compute multi level guided anchors
        guided_anchors_list = []
        loc_mask_list = []
        for img_id, _ in enumerate(img_metas):
            multi_level_guided_anchors = []
            multi_level_loc_mask = []
            for i in range(self.num_layers):
                squares = squares_list[img_id][i]
                shape_pred = shape_preds[i][img_id]
                loc_pred = loc_preds[i][img_id]
                guided_anchors, loc_mask = self._get_guided_anchors_single(
                    squares,
                    shape_pred,
                    loc_pred,
                    use_loc_filter=use_loc_filter)
                multi_level_guided_anchors.append(guided_anchors)
                multi_level_loc_mask.append(loc_mask)
            guided_anchors_list.append(multi_level_guided_anchors)
            loc_mask_list.append(multi_level_loc_mask)
        return squares_list, guided_anchors_list, loc_mask_list

    def _create_init_loc_targets(self, batch_size, featmap_sizes):
        all_loc_targets = []
        all_loc_weights = []
        all_ignore_map = []
        for h, w in featmap_sizes:
            loc_targets = ops.zeros(
                (batch_size, 1, h, w),
                ms.float32
            )
            loc_weights = np.full_like(loc_targets, -1)
            ignore_map = np.zeros_like(loc_targets)
            all_loc_targets.append(loc_targets)
            all_loc_weights.append(loc_weights)
            all_ignore_map.append(ignore_map)
        return all_loc_targets, all_loc_weights, all_ignore_map

    def ga_loc_targets(self, gt_bboxes_list, featmap_sizes):
        """
        Compute location targets for guided anchoring.

        Each feature map is divided into positive, negative and ignore regions.
        - positive regions: target 1, weight 1
        - ignore regions: target 0, weight 0
        - negative regions: target 0, weight 0.1

        Args:
        ----
            gt_bboxes_list (list[Tensor]): Gt bboxes of each image.
            featmap_sizes (list[tuple]): Multi level sizes of each feature
                maps.

        Returns:
        -------
            tuple
        """
        anchor_scale = self.approx_anchor_generator.octave_base_scale
        anchor_strides = self.approx_anchor_generator.strides
        # Currently only supports same stride in x and y direction.
        for stride in anchor_strides:
            assert stride[0] == stride[1]
        anchor_strides = [stride[0] for stride in anchor_strides]

        center_ratio = self.train_cfg['center_ratio']
        ignore_ratio = self.train_cfg['ignore_ratio']
        img_per_gpu = len(gt_bboxes_list)
        # Number of FPN levels
        num_lvls = 5  # len(featmap_sizes)
        r1 = (1 - center_ratio) / 2
        r2 = (1 - ignore_ratio) / 2

        all_loc_targets, all_loc_weights, all_ignore_map = \
            self._create_init_loc_targets(img_per_gpu, featmap_sizes)

        for img_id in range(img_per_gpu):
            gt_bboxes = gt_bboxes_list[img_id]
            scale = ops.sqrt((gt_bboxes[:, 2] - gt_bboxes[:, 0]) *
                             (gt_bboxes[:, 3] - gt_bboxes[:, 1]))
            min_anchor_size = np.full(
                (1,), float(anchor_scale * anchor_strides[0]))
            # assign gt bboxes to different feature levels w.r.t. their scales
            target_lvls = ops.floor(
                np.log2(scale) - np.log2(min_anchor_size) + 0.5)
            target_lvls = ops.clip_by_value(
                target_lvls, clip_value_min=0, clip_value_max=num_lvls - 1
            ).astype(ms.int64)
            for gt_id in range(gt_bboxes.shape[0]):
                lvl = target_lvls[gt_id]
                # rescaled to corresponding feature map
                gt_ = gt_bboxes[gt_id, :4] / anchor_strides[lvl]
                # calculate ignore regions
                ignore_x1, ignore_y1, ignore_x2, ignore_y2 = calc_region(
                    gt_, r2, featmap_sizes[lvl])
                # calculate positive (center) regions
                ctr_x1, ctr_y1, ctr_x2, ctr_y2 = calc_region(
                    gt_, r1, featmap_sizes[lvl])
                all_loc_targets[lvl][
                    img_id, 0, ctr_y1:ctr_y2 + 1, ctr_x1:ctr_x2 + 1
                ] = 1
                all_loc_weights[lvl][
                    img_id, 0, ignore_y1:ignore_y2 + 1, ignore_x1:ignore_x2 + 1
                ] = 0
                all_loc_weights[lvl][
                    img_id, 0, ctr_y1:ctr_y2 + 1, ctr_x1:ctr_x2 + 1
                ] = 1

                # calculate ignore map on nearby low level feature
                if lvl > 0:
                    d_lvl = lvl - 1
                    # rescaled to corresponding feature map
                    gt_ = gt_bboxes[gt_id, :4] / anchor_strides[d_lvl]
                    ignore_x1, ignore_y1, ignore_x2, ignore_y2 = calc_region(
                        gt_, r2, featmap_sizes[d_lvl])
                    all_ignore_map[d_lvl][img_id, 0, ignore_y1:ignore_y2 + 1,
                                          ignore_x1:ignore_x2 + 1] = 1
                # calculate ignore map on nearby high level feature
                if lvl < num_lvls - 1:
                    u_lvl = lvl + 1
                    # rescaled to corresponding feature map
                    gt_ = gt_bboxes[gt_id, :4] / anchor_strides[u_lvl]
                    ignore_x1, ignore_y1, ignore_x2, ignore_y2 = calc_region(
                        gt_, r2, featmap_sizes[u_lvl])
                    all_ignore_map[u_lvl][img_id, 0, ignore_y1:ignore_y2 + 1,
                                          ignore_x1:ignore_x2 + 1] = 1
        for lvl_id in range(num_lvls):
            # ignore negative regions w.r.t. ignore map
            all_loc_weights[lvl_id][(all_loc_weights[lvl_id] < 0)
                                    & (all_ignore_map[lvl_id] > 0)] = 0
            # set negative regions with weight 0.1
            all_loc_weights[lvl_id][all_loc_weights[lvl_id] < 0] = 0.1
        # loc average factor to balance loss
        loc_avg_factor = sum(
            [t.shape[0] * t.shape[-1] * t.shape[-2]
             for t in all_loc_targets]) / 200
        return all_loc_targets, all_loc_weights, loc_avg_factor

    def _ga_shape_target_single(self,
                                flat_approxs,
                                inside_flags,
                                flat_squares,
                                gt_bboxes,
                                gt_bboxes_ignore,
                                img_meta,
                                unmap_outputs=True):
        """
        Compute guided anchoring targets.

        This function returns sampled anchors and gt bboxes directly
        rather than calculates regression targets.

        Args:
        ----
            flat_approxs (Tensor): flat approxs of a single image,
                shape (n, 4)
            inside_flags (Tensor): inside flags of a single image,
                shape (n, ).
            flat_squares (Tensor): flat squares of a single image,
                shape (approxs_per_octave * n, 4)
            gt_bboxes (Tensor): Ground truth bboxes of a single image.
            img_meta: Meta info of a single image.
            approxs_per_octave (int): number of approxs per octave
            cfg (dict): RPN train configs.
            unmap_outputs (bool): unmap outputs or not.

        Returns:
        -------
            tuple
        """
        if not inside_flags.any():
            return (None,) * 5
        # assign gt and sample anchors
        expand_inside_flags = ops.broadcast_to(
            inside_flags[:, None], (-1, self.approxs_per_octave)
        ).reshape(-1)

        approxs_mask = ops.broadcast_to(
            expand_inside_flags[:, None], (-1, 4)
        )
        approxs = ops.masked_select(flat_approxs, approxs_mask)
        approxs = approxs.reshape((-1, 4))

        inside_flags_mask = ops.broadcast_to(inside_flags[:, None],
                                             (inside_flags.shape[0], 4))
        squares = ops.masked_select(flat_squares, inside_flags_mask)
        squares = squares.reshape((-1, 4))

        assign_result = self.ga_assigner.assign(approxs, squares,
                                                self.approxs_per_octave,
                                                gt_bboxes, gt_bboxes_ignore)
        sampling_result = self.ga_sampler.sample(assign_result, squares,
                                                 gt_bboxes)

        bbox_anchors = ops.zeros_like(squares)
        bbox_gts = ops.zeros_like(squares)
        bbox_weights = ops.zeros_like(squares)

        pos_inds = sampling_result.pos_inds
        neg_inds = sampling_result.neg_inds

        if pos_inds.shape[0] > 0:
            bbox_anchors[pos_inds] = sampling_result.pos_bboxes
            bbox_gts[pos_inds] = sampling_result.pos_gt_bboxes
            bbox_weights[pos_inds] = 1.0

        # map up to original set of anchors
        if unmap_outputs:
            num_total_anchors = flat_squares.shape[0]
            inside_flags = ops.nonzero(inside_flags).squeeze(1)
            bbox_anchors = unmap(bbox_anchors, num_total_anchors, inside_flags)
            bbox_gts = unmap(bbox_gts, num_total_anchors, inside_flags)
            bbox_weights = unmap(bbox_weights, num_total_anchors, inside_flags)

        return bbox_anchors, bbox_gts, bbox_weights, pos_inds, neg_inds

    def ga_shape_targets(self,
                         approx_list,
                         inside_flag_list,
                         square_list,
                         gt_bboxes_list,
                         img_metas,
                         gt_bboxes_ignore_list=None,
                         unmap_outputs=True):
        """
        Compute guided anchoring targets.

        Args:
        ----
            approx_list (list[list]): Multi level approxs of each image.
            inside_flag_list (list[list]): Multi level inside flags of each
                image.
            square_list (list[list]): Multi level squares of each image.
            gt_bboxes_list (list[Tensor]): Ground truth bboxes of each image.
            img_metas: Meta info of each image.
            gt_bboxes_ignore_list (list[Tensor]): ignore list of gt bboxes.
            unmap_outputs (bool): unmap outputs or not.

        Returns:
        -------
            tuple
        """
        num_imgs = len(img_metas)
        # anchor number of multi levels
        num_level_squares = [squares.shape[0] for squares in square_list[0]]
        # concat all level anchors and flags to a single tensor
        inside_flag_flat_list = []
        approx_flat_list = []
        square_flat_list = []
        for i in range(num_imgs):
            assert len(square_list[i]) == len(inside_flag_list[i])
            inside_flag_flat_list.append(ops.concat(inside_flag_list[i]))
            approx_flat_list.append(ops.concat(approx_list[i]))
            square_flat_list.append(ops.concat(square_list[i]))

        # compute targets for each image
        if gt_bboxes_ignore_list is None:
            gt_bboxes_ignore_list = [None for _ in range(num_imgs)]
        ga_shape_target_results = [
            self._ga_shape_target_single(
                approx_flat_list[i], inside_flag_flat_list[i],
                square_flat_list[i], gt_bboxes_list[i],
                gt_bboxes_ignore_list[i], img_metas[i],
                unmap_outputs=unmap_outputs)
            for i in range(num_imgs)
        ]

        all_bbox_anchors = [ga_shape_target_results[i][0]
                            for i in range(num_imgs)]
        all_bbox_gts = [ga_shape_target_results[i][1]
                        for i in range(num_imgs)]
        all_bbox_weights = [ga_shape_target_results[i][2]
                            for i in range(num_imgs)]
        pos_inds_list = [ga_shape_target_results[i][3]
                         for i in range(num_imgs)]
        neg_inds_list = [ga_shape_target_results[i][4]
                         for i in range(num_imgs)]

        # no valid anchors
        if any([bbox_anchors is None for bbox_anchors in all_bbox_anchors]):
            return None
        # sampled anchors of all images
        num_total_pos = sum([max(inds.numel(), 1) for inds in pos_inds_list])
        num_total_neg = sum([max(inds.numel(), 1) for inds in neg_inds_list])
        # split targets to a list w.r.t. multiple levels
        bbox_anchors_list = images_to_levels(all_bbox_anchors,
                                             num_level_squares)
        bbox_gts_list = images_to_levels(all_bbox_gts, num_level_squares)
        bbox_weights_list = images_to_levels(all_bbox_weights,
                                             num_level_squares)
        return (bbox_anchors_list, bbox_gts_list, bbox_weights_list,
                num_total_pos, num_total_neg)

    def loss_shape_single(self, shape_pred, bbox_anchors, bbox_gts,
                          anchor_weights, anchor_total_num):
        shape_pred = shape_pred.transpose((0, 2, 3, 1)).view((-1, 2))
        bbox_anchors = bbox_anchors.view((-1, 4))
        bbox_gts = bbox_gts.view((-1, 4))
        anchor_weights = anchor_weights.view((-1, 4))
        bbox_deltas = ops.zeros_like(bbox_anchors)
        bbox_deltas[:, 2:] += shape_pred
        # filter out negative samples to speed-up weighted_bounded_iou_loss
        inds = ops.nonzero(anchor_weights[:, 0] > 0)
        if inds.size > 0:
            inds = inds.squeeze(1)
        else:
            return ms.Tensor(0.0, dtype=ms.float32)
        bbox_deltas_ = bbox_deltas[inds]
        bbox_anchors_ = bbox_anchors[inds]
        bbox_gts_ = bbox_gts[inds]
        anchor_weights_ = anchor_weights[inds]
        pred_anchors_ = self.anchor_coder.decode(
            bbox_anchors_, bbox_deltas_, self.wh_ratio_clip)
        loss_shape = self.loss_shape(
            pred_anchors_,
            bbox_gts_,
            anchor_weights_,
            avg_factor=anchor_total_num)
        return loss_shape

    def loss_loc_single(self, loc_pred, loc_target, loc_weight,
                        loc_avg_factor):
        loss_loc = self.loss_loc(
            loc_pred.reshape(-1, 1),
            loc_target.reshape(-1).long(),
            loc_weight.reshape(-1),
            avg_factor=loc_avg_factor)
        return loss_loc

    def loss(self,
             cls_scores,
             bbox_preds,
             shape_preds,
             loc_preds,
             gt_bboxes,
             gt_labels,
             img_metas,
             gt_bboxes_ignore=None):
        featmap_sizes = [featmap.shape[-2:] for featmap in cls_scores]

        # get loc targets
        loc_targets, loc_weights, loc_avg_factor = self.ga_loc_targets(
            gt_bboxes, featmap_sizes)

        # get sampled approxes
        approxs_list, inside_flag_list = self.get_sampled_approxs(
            featmap_sizes, img_metas)
        # get squares and guided anchors
        squares_list, guided_anchors_list, _ = self.get_anchors(
            featmap_sizes, shape_preds, loc_preds, img_metas)

        # get shape targets
        shape_targets = self.ga_shape_targets(approxs_list, inside_flag_list,
                                              squares_list, gt_bboxes,
                                              img_metas)
        if shape_targets is None:
            return None
        (bbox_anchors_list, bbox_gts_list, anchor_weights_list, anchor_fg_num,
         anchor_bg_num) = shape_targets
        anchor_total_num = (
            anchor_fg_num if not self.ga_sampling else anchor_fg_num +
            anchor_bg_num)

        # get anchor targets
        label_channels = self.cls_out_channels if self.use_sigmoid_cls else 1
        cls_reg_targets = self.get_targets(
            guided_anchors_list,
            inside_flag_list,
            gt_bboxes,
            img_metas,
            gt_bboxes_ignore_list=gt_bboxes_ignore,
            gt_labels_list=gt_labels,
            label_channels=label_channels)
        if cls_reg_targets is None:
            return None
        (labels_list, label_weights_list, bbox_targets_list, bbox_weights_list,
         num_total_pos, num_total_neg) = cls_reg_targets
        num_total_samples = (
            num_total_pos + num_total_neg if self.sampling else num_total_pos)

        # anchor number of multi levels
        num_level_anchors = [
            anchors.shape[0] for anchors in guided_anchors_list[0]
        ]
        # concat all level anchors to a single tensor
        concat_anchor_list = []
        for i in range(len(guided_anchors_list)):
            concat_anchor_list.append(ops.concat(guided_anchors_list[i]))
        all_anchor_list = images_to_levels(concat_anchor_list,
                                           num_level_anchors)

        # get classification and bbox regression losses
        losses_cls, losses_bbox = multi_apply(
            self.loss_single,
            [
                cls_scores,
                bbox_preds,
                all_anchor_list,
                labels_list,
                label_weights_list,
                bbox_targets_list,
                bbox_weights_list
            ],
            num_total_samples=num_total_samples)

        # get anchor location loss
        losses_loc = []
        for i in range(len(shape_preds)):
            loss_loc = self.loss_loc_single(
                loc_preds[i],
                loc_targets[i],
                loc_weights[i],
                loc_avg_factor=loc_avg_factor)
            losses_loc.append(loss_loc)

        # get anchor shape loss
        losses_shape = []
        # Iterate over FPN levels and compute ShapeLoss (used BoundedIoULoss)
        for i in range(len(shape_preds)):
            # Compute loss for i-th FPN level
            loss_shape = self.loss_shape_single(
                shape_preds[i],
                bbox_anchors_list[i],
                bbox_gts_list[i],
                anchor_weights_list[i],
                anchor_total_num=anchor_total_num)
            losses_shape.append(loss_shape)

        return dict(
            loss_cls=losses_cls,
            loss_bbox=losses_bbox,
            loss_shape=losses_shape,
            loss_loc=losses_loc
        )

    def _get_targets_single(self,
                            flat_anchors,
                            valid_flags,
                            gt_bboxes,
                            gt_bboxes_ignore,
                            gt_labels,
                            img_meta,
                            label_channels=1,
                            unmap_outputs=True):
        """
        Compute regression and classification targets for anchors in a
        single image.

        Args:
        ----
            flat_anchors (Tensor): Multi-level anchors of the image, which are
                concatenated into a single tensor of shape (num_anchors ,4)
            valid_flags (Tensor): Multi level valid flags of the image,
                which are concatenated into a single tensor of
                    shape (num_anchors,).
            gt_bboxes (Tensor): Ground truth bboxes of the image,
                shape (num_gts, 4).
            gt_bboxes_ignore (Tensor): Ground truth bboxes to be
                ignored, shape (num_ignored_gts, 4).
            img_meta (dict): Meta info of the image.
            gt_labels (Tensor): Ground truth labels of each box,
                shape (num_gts,).
            label_channels (int): Channel of label.
            unmap_outputs (bool): Whether to map outputs back to the original
                set of anchors.

        Returns:
        -------
            tuple:
                labels_list (list[Tensor]): Labels of each level
                label_weights_list (list[Tensor]): Label weights of each level
                bbox_targets_list (list[Tensor]): BBox targets of each level
                bbox_weights_list (list[Tensor]): BBox weights of each level
                num_total_pos (int): Number of positive samples in all images
                num_total_neg (int): Number of negative samples in all images
        """
        inside_flags = anchor_inside_flags(flat_anchors, valid_flags,
                                           img_meta[:2],
                                           -1)
        # In Mindspore implementation here inside_flags are all True
        if not inside_flags.any():
            return (None,) * 7
        # assign gt and sample anchors
        anchors = flat_anchors[inside_flags, :]

        assign_result = self.assigner.assign(
            anchors, gt_bboxes, gt_bboxes_ignore,
            None if self.sampling else gt_labels)
        sampling_result = self.sampler.sample(assign_result, anchors,
                                              gt_bboxes)

        num_valid_anchors = anchors.shape[0]
        bbox_targets = ops.ZerosLike()(anchors)
        bbox_weights = ops.ZerosLike()(anchors)
        labels = np.full((num_valid_anchors,),
                         self.num_classes,
                         dtype=ms.int64
                         )
        label_weights = ops.zeros(num_valid_anchors, dtype=ms.float32)

        pos_inds = sampling_result.pos_inds
        neg_inds = sampling_result.neg_inds
        if pos_inds.size > 0:
            if not self.reg_decoded_bbox:
                pos_bbox_targets = self.bbox_coder.encode(
                    sampling_result.pos_bboxes, sampling_result.pos_gt_bboxes)
            else:
                pos_bbox_targets = sampling_result.pos_gt_bboxes
            bbox_targets[pos_inds, :] = pos_bbox_targets
            bbox_weights[pos_inds, :] = 1.0
            if gt_labels is None:
                # Only rpn gives gt_labels as None
                # Foreground is the first class since v2.5.0
                labels[pos_inds] = 0
            else:
                labels[pos_inds] = gt_labels[
                    sampling_result.pos_assigned_gt_inds, 0]
            if self.train_cfg['pos_weight'] <= 0:
                label_weights[pos_inds] = 1.0
            else:
                label_weights[pos_inds] = self.train_cfg['pos_weight']
        if neg_inds.size > 0:
            label_weights[neg_inds] = 1.0

        # map up to original set of anchors
        if unmap_outputs:
            num_total_anchors = flat_anchors.shape[0]
            labels = unmap(
                labels, num_total_anchors, inside_flags,
                fill=self.num_classes)  # fill bg label
            label_weights = unmap(label_weights, num_total_anchors,
                                  inside_flags)
            bbox_targets = unmap(bbox_targets, num_total_anchors, inside_flags)
            bbox_weights = unmap(bbox_weights, num_total_anchors, inside_flags)

        return (labels, label_weights, bbox_targets, bbox_weights, pos_inds,
                neg_inds, sampling_result)

    def get_targets(self,
                    anchor_list,
                    valid_flag_list,
                    gt_bboxes_list,
                    img_metas,
                    gt_bboxes_ignore_list=None,
                    gt_labels_list=None,
                    label_channels=1,
                    unmap_outputs=True,
                    return_sampling_results=False):
        """
        Compute regression and classification targets for anchors in
        multiple images.

        Args:
        ----
            anchor_list (list[list[Tensor]]): Multi level anchors of each
                image. The outer list indicates images, and the inner list
                corresponds to feature levels of the image. Each element of
                the inner list is a tensor of shape (num_anchors, 4).
            valid_flag_list (list[list[Tensor]]): Multi level valid flags of
                each image. The outer list indicates images, and the inner list
                corresponds to feature levels of the image. Each element of
                the inner list is a tensor of shape (num_anchors, )
            gt_bboxes_list (list[Tensor]): Ground truth bboxes of each image.
            img_metas: Meta info of each image.
            gt_bboxes_ignore_list (list[Tensor]): Ground truth bboxes to be
                ignored.
            gt_labels_list (list[Tensor]): Ground truth labels of each box.
            label_channels (int): Channel of label.
            unmap_outputs (bool): Whether to map outputs back to the original
                set of anchors.

        Returns:
        -------
            tuple: Usually returns a tuple containing learning targets.

                - labels_list (list[Tensor]): Labels of each level.
                - label_weights_list (list[Tensor]): Label weights of each
                  level.
                - bbox_targets_list (list[Tensor]): BBox targets of each level.
                - bbox_weights_list (list[Tensor]): BBox weights of each level.
                - num_total_pos (int): Number of positive samples in all
                  images.
                - num_total_neg (int): Number of negative samples in all
                  images.

            additional_returns: This function enables user-defined returns from
                `self._get_targets_single`. These returns are currently refined
                to properties at each feature map (i.e. having HxW dimension).
                The results will be concatenated after the end
        """
        num_imgs = len(img_metas)
        assert len(anchor_list) == len(valid_flag_list)
        assert len(anchor_list) == num_imgs

        # anchor number of multi levels
        num_level_anchors = [anchors.shape[0] for anchors in anchor_list[0]]
        # concat all level anchors to a single tensor
        concat_anchor_list = []
        concat_valid_flag_list = []
        for i in range(num_imgs):
            assert len(anchor_list[i]) == len(valid_flag_list[i])
            concat_anchor_list.append(ops.Concat()(anchor_list[i]))
            concat_valid_flag_list.append(ops.Concat()(valid_flag_list[i]))

        # compute targets for each image
        if gt_bboxes_ignore_list is None:
            gt_bboxes_ignore_list = [None for _ in range(num_imgs)]
        if gt_labels_list is None:
            gt_labels_list = [None for _ in range(num_imgs)]
        results = multi_apply(
            self._get_targets_single,
            [concat_anchor_list,
             concat_valid_flag_list,
             gt_bboxes_list,
             gt_bboxes_ignore_list,
             gt_labels_list,
             img_metas],
            label_channels=label_channels,
            unmap_outputs=unmap_outputs)
        (all_labels, all_label_weights, all_bbox_targets, all_bbox_weights,
         pos_inds_list, neg_inds_list, sampling_results_list) = results[:7]
        rest_results = list(results[7:])  # user-added return values
        # no valid anchors
        if any([labels is None for labels in all_labels]):
            return None
        # sampled anchors of all images
        num_total_pos = sum([max(inds.numel(), 1) for inds in pos_inds_list])
        num_total_neg = sum([max(inds.numel(), 1) for inds in neg_inds_list])
        # split targets to a list w.r.t. multiple levels
        labels_list = images_to_levels(all_labels, num_level_anchors)
        label_weights_list = images_to_levels(all_label_weights,
                                              num_level_anchors)
        bbox_targets_list = images_to_levels(all_bbox_targets,
                                             num_level_anchors)
        bbox_weights_list = images_to_levels(all_bbox_weights,
                                             num_level_anchors)
        res = (labels_list, label_weights_list, bbox_targets_list,
               bbox_weights_list, num_total_pos, num_total_neg)
        if return_sampling_results:
            res = res + (sampling_results_list,)
        for i, r in enumerate(rest_results):  # user-added return values
            rest_results[i] = images_to_levels(r, num_level_anchors)

        return res + tuple(rest_results)

    def forward_train(self,
                      x,
                      img_metas,
                      gt_bboxes,
                      gt_labels=None,
                      gt_bboxes_ignore=None,
                      proposal_cfg=None,
                      **kwargs):
        """
        Forward train method.

        Args:
        ----
            x (list[Tensor]): Features from FPN.
            img_metas: Meta information of each image, e.g.,
                image size, scaling factor, etc.
            gt_bboxes (Tensor): Ground truth bboxes of the image,
                shape (num_gts, 4).
            gt_labels (Tensor): Ground truth labels of each box,
                shape (num_gts,).
            gt_bboxes_ignore (Tensor): Ground truth bboxes to be
                ignored, shape (num_ignored_gts, 4).
            proposal_cfg (mmcv.Config): Test / postprocessing configuration,
                if None, test_cfg would be used.
            kwargs:
                other kwargs

        Returns:
        -------
            tuple:
                losses: (dict[str, Tensor]): A dictionary of loss components.
                proposal_list (list[Tensor]): Proposals of each image.
        """
        outs = self.construct([x])
        if gt_labels is None:
            loss_inputs = outs + (gt_bboxes, img_metas)
        else:
            loss_inputs = outs + (gt_bboxes, gt_labels, img_metas)
        losses = self.loss(*loss_inputs, gt_bboxes_ignore=gt_bboxes_ignore)
        if proposal_cfg is None:
            return losses
        proposal_list = self.get_bboxes(
            *outs, img_metas=img_metas, cfg=proposal_cfg)
        return losses, proposal_list
