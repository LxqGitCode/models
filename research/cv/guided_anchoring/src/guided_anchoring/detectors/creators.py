# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
from typing import Union

from ...config import Config
from .faster_rcnn import FasterRCNN
from .retina_net import RetinaNet
from ..heads import create_head
from ..backbones import create_backbone
from ..necks import create_neck
from ..roi_heads import create_standard_roi_head


def create_model(config: Config) -> Union[RetinaNet, FasterRCNN]:
    backbone = create_backbone(config.backbone)
    neck = create_neck(config.neck)
    if config.type == 'RetinaNet':
        bbox_head = create_head(config.bbox_head, config.test_cfg,
                                config.get('train_cfg'))
        return RetinaNet(
            backbone, neck, bbox_head
        )
    if config.type == 'FasterRCNN':
        rpn_head = create_head(config.rpn_head, config.test_cfg,
                               config.get('train_cfg'))
        roi_head = create_standard_roi_head(config.roi_head, config.test_cfg,
                                            config.get('train_cfg'))
        return FasterRCNN(
            backbone=backbone,
            neck=neck,
            rpn_head=rpn_head,
            roi_head=roi_head,
            train_cfg=config.test_cfg,
            test_cfg=config.get('train_cfg'),
        )
    raise RuntimeError(f'Unknown model type: {config.type}')
