# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
#
# This file has been derived from the
# https://github.com/open-mmlab/mmdetection/tree/v2.28.2
# repository and modified.
# ============================================================================
import mindspore.nn as nn


class FasterRCNN(nn.Cell):
    """Implementation of `Faster R-CNN <https://arxiv.org/abs/1506.01497>`_."""

    def __init__(self,
                 backbone,
                 neck,
                 rpn_head,
                 roi_head,
                 train_cfg,
                 test_cfg,
                 pretrained=None):
        super().__init__()
        self.backbone = backbone
        self.neck = neck
        self.rpn_head = rpn_head
        self.roi_head = roi_head
        self.train_cfg = train_cfg
        self.test_cfg = test_cfg

    def construct(self, img_data, img_metas,
                  gt_bboxes=None, gt_labels=None, gt_bboxes_ignore=None):
        """
        construct the FasterRCNN Network.

        Args:
        ----
            img_data: input image data.
            img_metas: meta label of img.
            gt_bboxes (Tensor): get the value of bboxes.
            gt_labels (Tensor): get the value of labels.
            gt_bboxes_ignore (Tensor): specify which bounding
                boxes can be ignored when computing the loss.

        Returns:
        -------
            Tuple,tuple of output tensor
        """
        if self.training:
            output = self.forward_train(
                img_data, img_metas, gt_bboxes, gt_labels, gt_bboxes_ignore
            )
        else:
            output = self.simple_test(img_data, img_metas)

        return output

    def simple_test(self, img, img_metas, proposals=None, rescale=False):
        """Test without augmentation."""
        x = self.extract_feat(img)
        proposal_list = self.rpn_head.simple_test_rpn(x, img_metas)

        bboxes, labels = self.roi_head.simple_test(
            x, proposal_list, img_metas, rescale=rescale)
        return [[bboxes, labels]]

    def extract_feat(self, img):
        """Directly extract features from the backbone+neck."""
        x = self.backbone(img)
        x = self.neck(x)
        return x

    def forward_train(self,
                      img,
                      img_metas,
                      gt_bboxes,
                      gt_labels,
                      gt_bboxes_ignore=None,
                      proposals=None,
                      **kwargs):
        """
        Args:
        ----
            img (Tensor): of shape (N, C, H, W) encoding input images.
                Typically these should be mean centered and std scaled.
            img_metas: list of image info (image size).
            gt_bboxes (list[Tensor]): Ground truth bboxes for each image with
                shape (num_gts, 4) in [tl_x, tl_y, br_x, br_y] format.
            gt_labels (list[Tensor]): class indices corresponding to each box
            gt_bboxes_ignore (None | list[Tensor]): specify which bounding
                boxes can be ignored when computing the loss.

        Returns:
        -------
            dict[str, Tensor]: a dictionary of loss components
        """
        x = self.extract_feat(img)

        losses = dict()

        # RPN forward and loss
        proposal_cfg = self.train_cfg.get('rpn_proposal',
                                          self.test_cfg.rpn)

        rpn_losses, _ = self.rpn_head.forward_train(
            x,
            img_metas,
            gt_bboxes,
            gt_bboxes_ignore=gt_bboxes_ignore,
            proposal_cfg=proposal_cfg,
            **kwargs)
        losses.update(rpn_losses)
        return losses
