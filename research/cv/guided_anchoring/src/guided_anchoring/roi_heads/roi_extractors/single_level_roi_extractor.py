# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
#
# This file has been derived from the
# https://github.com/open-mmlab/mmdetection/tree/v2.28.2
# repository and modified.
# ============================================================================
from typing import Any, Sequence

import mindspore as ms
import mindspore.nn as nn
import mindspore.ops as ops
import numpy as np

from ...ops import ROIAlign


class SingleRoIExtractor(nn.Cell):
    """
    Extract RoI features from a single level feature map.

    If there are multiple input feature levels, each RoI is mapped to a level
    according to its scale.

    Args:
    ----
        roi_layer (Config): Specify RoI layer type and arguments.
        out_channels (int): Output channels of RoI layers.
        featmap_strides (Sequence[int]): Strides of input feature maps.
        train_batch_size (int): Batch size in training mode.
        test_batch_size (int): Batch size in test mode.
        finest_scale (int): Scale threshold of mapping to level 0.
    """

    def __init__(
            self,
            roi_layer: Any,
            out_channels: int,
            featmap_strides: Sequence[int],
            train_batch_size: int = 1,
            test_batch_size: int = 1,
            finest_scale: int = 56
    ):
        """Init SingleRoIExtractor."""
        super().__init__()
        self.train_batch_size = train_batch_size
        self.test_batch_size = test_batch_size
        self.out_channels = out_channels
        self.featmap_strides = featmap_strides
        self.num_levels = len(self.featmap_strides)
        self.out_size = roi_layer.output_size
        self.sampling_ratio = roi_layer.sampling_ratio
        self.roi_layers = self.build_roi_layers(self.featmap_strides)
        self.roi_layers = nn.layer.CellList(self.roi_layers)

        self.finest_scale_ = finest_scale

        self.dtype = np.float32
        self.ms_dtype = ms.float32
        self.set_train_local(training=True)

    def set_train_local(self, training: bool = True):
        """Set training flag."""
        self.training_local = training

        # Init Tensor
        self.batch_size = (
            self.train_batch_size if self.training_local
            else self.test_batch_size
        )
        self.ones = ms.Tensor(
            np.array(np.ones((self.batch_size, 1)), dtype=self.dtype)
        )
        finest_scale = np.array(
            np.ones((self.batch_size, 1)), dtype=self.dtype
        ) * self.finest_scale_
        self.finest_scale = ms.Tensor(finest_scale)
        self.epslion = ms.Tensor(
            np.array(np.ones((self.batch_size, 1)), dtype=self.dtype) *
            self.dtype(1e-6)
        )
        self.zeros = ms.Tensor(
            np.array(np.zeros((self.batch_size, 1)), dtype=np.int32)
        )
        self.max_levels = ms.Tensor(
            np.array(np.ones((self.batch_size, 1)), dtype=np.int32) *
            (self.num_levels - 1)
        )
        self.res_ = ms.Tensor(
            np.array(
                np.zeros((self.batch_size, self.out_channels, self.out_size,
                          self.out_size)),
                dtype=self.dtype
            )
        )

    def num_inputs(self):
        """Number of input feature maps."""
        return len(self.featmap_strides)

    def build_roi_layers(
            self, featmap_strides: Sequence[int]
    ) -> Sequence[ms.nn.Cell]:
        """Build roi_align layers."""
        roi_layers = []
        for s in featmap_strides:
            layer_cls = ROIAlign(output_size=self.out_size,
                                 spatial_scale=1 / s,
                                 sampling_ratio=self.sampling_ratio)
            roi_layers.append(layer_cls)
        return roi_layers

    def map_roi_levels(self, rois: ms.Tensor) -> ms.Tensor:
        """
        Map rois to corresponding feature levels by scales.

        - scale < finest_scale * 2: level 0
        - finest_scale * 2 <= scale < finest_scale * 4: level 1
        - finest_scale * 4 <= scale < finest_scale * 8: level 2
        - scale >= finest_scale * 8: level 3

        Args:
        ----
            rois (ms.Tensor): Input RoIs, shape (k, 5).

        Returns:
        -------
            ms.Tensor: Level index (0-based) of each RoI, shape (k, )
        """
        scale = ms.ops.sqrt(
            (rois[:, 3:4] - rois[:, 1:2]) * (rois[:, 4:5] - rois[:, 2:3])
        )
        target_lvls = ops.log2(scale / self.finest_scale + self.epslion)
        target_lvls = ops.floor(target_lvls)
        target_lvls = ms.ops.cast(target_lvls, ms.int32)
        target_lvls = ms.ops.clip_by_value(
            target_lvls, clip_value_min=self.zeros,
            clip_value_max=self.max_levels
        )
        return target_lvls

    def construct(
            self, feats: Sequence[ms.Tensor], rois: ms.Tensor
    ) -> ms.Tensor:
        """Extract features."""
        target_lvls = self.map_roi_levels(rois)

        roi_feats_t = self.roi_layers[0](feats[0], rois)
        res = ops.zeros_like(roi_feats_t)
        for i in range(self.num_levels):
            mask = ops.equal(target_lvls, ops.scalar_to_tensor(i, ms.int32))
            mask = ops.reshape(mask, (-1, 1, 1, 1))

            mask = ops.cast(
                ops.tile(
                    ops.cast(mask, ms.int32),
                    (1, self.out_channels, self.out_size, self.out_size)
                ),
                ms.bool_
            )
            res = ops.select(mask, roi_feats_t, res)

            if i + 1 < len(self.roi_layers):
                roi_feats_t = self.roi_layers[i + 1](feats[i + 1], rois)

        return res
