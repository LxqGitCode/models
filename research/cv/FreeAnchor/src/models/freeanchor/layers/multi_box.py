# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
#
# This file has been derived from the https://github.com/open-mmlab/mmdetection/tree/v2.28.2
# repository and modified.
# ============================================================================
"""RetinaNet dense bounding box head (regression, classification)."""
import mindspore.nn as nn
from mindspore.ops import operations as P
from mindspore.ops import functional as F


class ConvWrapper(nn.Cell):
    def __init__(self, conv):
        super(ConvWrapper, self).__init__()
        self.conv = conv

    def construct(self, *inputs, **kwargs):
        return self.conv(*inputs, **kwargs)


def ClassificationModel(in_channel, feature_size=256):
    conv1 = ConvWrapper(nn.Conv2d(in_channel, feature_size, kernel_size=3, pad_mode='same', has_bias=True))
    conv2 = ConvWrapper(nn.Conv2d(feature_size, feature_size, kernel_size=3, pad_mode='same', has_bias=True))
    conv3 = ConvWrapper(nn.Conv2d(feature_size, feature_size, kernel_size=3, pad_mode='same', has_bias=True))
    conv4 = ConvWrapper(nn.Conv2d(feature_size, feature_size, kernel_size=3, pad_mode='same', has_bias=True))
    return nn.CellList([conv1, conv2, conv3, conv4])


def RegressionModel(in_channel, feature_size=256):
    conv1 = ConvWrapper(nn.Conv2d(in_channel, feature_size, kernel_size=3, pad_mode='same', has_bias=True))
    conv2 = ConvWrapper(nn.Conv2d(feature_size, feature_size, kernel_size=3, pad_mode='same', has_bias=True))
    conv3 = ConvWrapper(nn.Conv2d(feature_size, feature_size, kernel_size=3, pad_mode='same', has_bias=True))
    conv4 = ConvWrapper(nn.Conv2d(feature_size, feature_size, kernel_size=3, pad_mode='same', has_bias=True))
    return nn.CellList([conv1, conv2, conv3, conv4])


class FlattenConcat(nn.Cell):
    """
    Concatenate predictions into a single tensor.

    Args:
        config (dict): The default config of retinanet.

    Returns:
        Tensor, flatten predictions.
    """
    def __init__(self, config):
        super(FlattenConcat, self).__init__()
        self.num_retinanet_boxes = config.num_retinanet_boxes
        self.concat = P.Concat(axis=1)
        self.transpose = P.Transpose()
    def construct(self, inputs):
        output = ()
        batch_size = F.shape(inputs[0])[0]
        for x in inputs:
            x = self.transpose(x, (0, 2, 3, 1))
            # print(x.shape)
            output += (F.reshape(x, (batch_size, -1)),)
        res = self.concat(output)
        return F.reshape(res, (batch_size, self.num_retinanet_boxes, -1))


class MultiBox(nn.Cell):
    """
    Multibox conv layers. Each multibox layer contains class conf scores and localization predictions.

    Args:
        config (dict): The default config of retinanet.

    Returns:
        Tensor, localization predictions.
        Tensor, class conf scores.
    """
    def __init__(self, config):
        super(MultiBox, self).__init__()

        out_channels = config.extras_out_channels
        num_default = config.num_default
        num_classes = config.bbox_head.num_classes
        self.reg_convs = RegressionModel(in_channel=out_channels[0])
        self.cls_convs = ClassificationModel(in_channel=out_channels[0])

        self.act = nn.ReLU()
        self.flatten_concat = FlattenConcat(config)
        feat_channels = 256
        cls_out_channels = num_classes
        self.retina_cls = nn.Conv2d(
            feat_channels,
            num_default[0] * cls_out_channels,  # 720
            3,
            pad_mode='pad',
            padding=1,
            has_bias=True
        )
        self.retina_reg = nn.Conv2d(
            feat_channels,
            num_default[0] * 4,  # num_base_priors * 4,
            3, pad_mode='pad', padding=1,
            has_bias=True
        )

    def construct(self, inputs):
        res_cls = []
        res_reg = []
        for k in range(len(inputs)):
            cls_feat = inputs[k]
            for j in range(len(self.cls_convs)):
                cls_feat = self.cls_convs[j](cls_feat)
                cls_feat = self.act(cls_feat)
            cls_feat = self.retina_cls(cls_feat)
            res_cls.append(cls_feat)
        for k in range(len(inputs)):
            reg_feat = inputs[k]
            for j in range(len(self.reg_convs)):
                reg_feat = self.reg_convs[j](reg_feat)
                reg_feat = self.act(reg_feat)
            reg_feat = self.retina_reg(reg_feat)
            res_reg.append(reg_feat)

        return res_reg, res_cls
