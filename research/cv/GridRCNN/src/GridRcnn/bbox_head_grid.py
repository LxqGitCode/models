# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
#
# This file has been derived from the https://github.com/STVIR/Grid-R-CNN
# repository and modified.
# ============================================================================
"""RCNN block for Grid R-CNN."""
import mindspore as ms
from mindspore import nn
import numpy as np


class BBoxHeadGrid(nn.Cell):
    """RCNN block for Grid RCNN.

    Args:
        cfg (Config): configuration object
        num_fcs (int): number of layers before classification.
        roi_feat_size (int): h and w size of input feature maps.
        in_channels (int): number of input channels
        num_classes (int): number of classification categories
            (including background)
        fc_out_channels (int): number of output channels of layer before
            classification.
        loss_cls_weight (float): classification loss weight.
    """

    def __init__(
            self,
            cfg,
            num_fcs=2,
            roi_feat_size=7,
            in_channels=256,
            num_classes=81,
            fc_out_channels=1024,
            loss_cls_weight=1,
    ):
        super().__init__()
        self.ms_type = ms.float32
        self.dtype = np.float32
        self.loss_cls_weight = ms.Tensor(
            np.array(cfg.rcnn_loss_cls_weight).astype(self.dtype)
        )
        self.train_batch_size = cfg.batch_size
        self.test_batch_size = cfg.test_batch_size

        self.train_num_bboxes = self.train_batch_size * (
            cfg.num_expected_pos_stage2 + cfg.num_expected_neg_stage2
        )

        self.loss_cls_weight = loss_cls_weight

        self.roi_feat_size = roi_feat_size
        self.in_channels = in_channels
        self.num_classes = num_classes

        self.num_shared_fcs = num_fcs
        self.fc_out_channels = fc_out_channels

        # add shared convs and fcs
        self.shared_fcs, last_layer_dim = (
            self._add_fc_branch(self.num_shared_fcs, self.in_channels)
        )
        self.shared_out_channels = last_layer_dim

        self.cls_last_dim = last_layer_dim
        self.reg_last_dim = last_layer_dim

        # reconstruct fc_cls and fc_reg since input channels are changed
        self.fc_cls = nn.Dense(self.cls_last_dim, self.num_classes)

        self.loss_cls = ms.ops.SoftmaxCrossEntropyWithLogits()

        self.onehot = ms.ops.OneHot()
        self.sum_loss = ms.ops.ReduceSum()
        self.on_value = ms.Tensor(1.0, ms.float32)
        self.off_value = ms.Tensor(0.0, ms.float32)

        self.tile = ms.ops.Tile()
        self.expandims = ms.ops.ExpandDims()
        self.logicaland = ms.ops.LogicalAnd()
        self.greater = ms.ops.Greater()
        self.reshape = ms.ops.Reshape()

        self.relu = nn.ReLU()
        self.flatten = nn.Flatten()

    def _add_fc_branch(self, num_branch_fcs, in_channels):
        """Add shared or separable branch

        convs -> avg pool (optional) -> fcs
        """
        last_layer_dim = in_channels
        # add branch specific fc layers
        branch_fcs = nn.CellList()
        # for shared branch, only consider self.with_avg_pool
        # for separated branches, also consider self.num_shared_fcs
        last_layer_dim *= (self.roi_feat_size * self.roi_feat_size)
        for i in range(num_branch_fcs):
            fc_in_channels = (
                last_layer_dim if i == 0 else self.fc_out_channels)
            branch_fcs.append(
                nn.Dense(fc_in_channels, self.fc_out_channels)
            )
        last_layer_dim = self.fc_out_channels
        return branch_fcs, last_layer_dim

    def construct(self, feature_map, labels, mask):
        """Compute outputs of classification loss.

        Args:
            feature_map (Tensor): input feature map.
            labels (Optional(Tensor)): GT labels (for loss calculation).
            mask (Optional(Tensor)): GT mask (for loss calculation).

        Returns:
            Classification outputs Tensor or loss.
        """
        # shared part
        x = feature_map
        x = self.flatten(x)
        for layer in self.shared_fcs:
            x = self.relu(layer(x))
        # separate branches
        x_cls = x

        cls_score = self.fc_cls(x_cls)
        if self.training:
            labels = self.onehot(labels, self.num_classes, self.on_value,
                                 self.off_value)
            loss_cls = self.loss(
                cls_score, labels, mask
            )
            out = loss_cls
        else:
            out = cls_score
        return out

    def loss(self, cls_score, labels, weights):
        """Loss method."""
        loss_cls, _ = self.loss_cls(cls_score, labels)

        weights = self.cast(weights, self.ms_type)
        loss_cls = loss_cls * weights
        loss_cls = self.sum_loss(loss_cls, (0,)) / self.sum_loss(weights, (0,))
        loss_cls = loss_cls * self.loss_cls_weight
        return loss_cls
