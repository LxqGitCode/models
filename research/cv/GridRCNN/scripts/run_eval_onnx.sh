#!/bin/bash
# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================

if [ $# != 4 ] && [ $# != 5 ]
then
    echo "Usage:
    bash scripts/run_eval_onnx.sh [CONFIG_PATH] [VAL_DATA] [ONNX_PATH] [DEVICE_TARGET] (Optional)[PREDICTION_PATH]
    "
exit 1
fi

get_real_path(){
  if [ "${1:0:1}" == "/" ]; then
    echo "$1"
  else
    echo "$(realpath -m $PWD/$1)"
  fi
}

CONFIG_PATH=$(get_real_path $1)
VAL_DATA=$(get_real_path $2)
ONNX_PATH=$(get_real_path $3)
DEVICE_TARGET=$4

echo "config_path: $CONFIG_PATH"
echo "val_dataset: $VAL_DATA"
echo "onnx_path: $ONNX_PATH"
echo "device_target: $DEVICE_TARGET"

if [ $# -eq 5 ]
then
  PREDICTION_PATH=$(get_real_path $5)
  echo "prediction_path: $PREDICTION_PATH"
fi


if [ $# -eq 5 ]
then
  python eval_onnx.py --config_path $CONFIG_PATH --onnx_path $ONNX_PATH --device_target $DEVICE_TARGET \
  --val_dataset $VAL_DATA --val_data_type mindrecord --prediction_path $PREDICTION_PATH &> eval_onnx.log &
fi
if [ $# -eq 4 ]
then
  python eval_onnx.py --config_path $CONFIG_PATH --onnx_path $ONNX_PATH --device_target $DEVICE_TARGET \
  --val_dataset $VAL_DATA --val_data_type mindrecord &> eval_onnx.log &
fi