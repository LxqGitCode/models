# Gold-YOLO : YOLO with Gather-and-Distribute Mechanism

<img src="assets/model.png" style="zoom: 9%;" />

## Benchmark

<img src="assets/fps-7.png" style="zoom: 33%;" /> <img src="assets/fps-8.png" style="zoom:33%;" />

### benchmark and pretrained weights

| Model       | Distill | Pre-Train | mAP-all @ 0.50:0.95 | mAP-all @ 0.50 | mAP-all @ 0.75 | mAP-small @ 0.50:0.95 | mAP-medium @ 0.50:0.95 | mAP-large @ 0.50:0.95 | MindSpore Checkpoint                                                                                                                   |
|-------------|:-------:|:---------:|:-------------------:|:--------------:|:--------------:|:---------------------:|:----------------------:|:---------------------:|----------------------------------------------------------------------------------------------------------------------------------------|
| Gold-YOLO-N |    ✅    |     ❌     |        39.9         |      56.0      |      43.3      |         19.5          |          44.2          |         57.9          | [Gold_n-dist-state_dict_ms.ckpt](https://download.mindspore.cn/model_zoo/research/cv/Gold_YOLO/Gold_n-dist-state_dict_ms.ckpt)         |
| Gold-YOLO-S |    ✅    |     ✅     |        46.5         |      63.5      |      50.4      |         26.0          |          51.4          |         63.7          | [Gold_s-pre-dist-state_dict_ms.ckpt](https://download.mindspore.cn/model_zoo/research/cv/Gold_YOLO/Gold_s-pre-dist-state_dict_ms.ckpt) |
| Gold-YOLO-M |    ✅    |     ✅     |        51.1         |      68.5      |      55.4      |         32.4          |          56.2          |         68.5          | [Gold_m-pre-dist-state_dict_ms.ckpt](https://download.mindspore.cn/model_zoo/research/cv/Gold_YOLO/Gold_m-pre-dist-state_dict_ms.ckpt) |
| Gold-YOLO-L |    ✅    |     ✅     |        53.2         |      71.0      |      58.3      |         34.0          |          58.8          |         70.0          | [Gold_l-pre-dist-state_dict_ms.ckpt](https://download.mindspore.cn/model_zoo/research/cv/Gold_YOLO/Gold_l-pre-dist-state_dict_ms.ckpt) |

## install requirements

```shell
pip install -r requirements.txt
```

## eval

1. change the data/coco.yaml to your data path

2. download the [MindSpore Checkpoint](#benchmark-and-pretrained-weights) from Benchmark to the pretrain fold

3. eval Gold-YOLO

   ```shell
   # eval Gold-YOLO-n
   python tools/eval.py --reproduce_640_eval --data data/coco.yaml --conf_file configs/gold_yolo-n.py --weights pretrain/Gold_n-dist-state_dict_ms.ckpt

   # eval Gold-YOLO-s
   python tools/eval.py --reproduce_640_eval --data data/coco.yaml --conf_file configs/gold_yolo-s.py --weights pretrain/Gold_s-pre-dist-state_dict_ms.ckpt

   # eval Gold-YOLO-m
   python tools/eval.py --reproduce_640_eval --data data/coco.yaml --conf_file configs/gold_yolo-m.py --weights pretrain/Gold_m-pre-dist-state_dict_ms.ckpt

   # eval Gold-YOLO-l
   python tools/eval.py --reproduce_640_eval --data data/coco.yaml --conf_file configs/gold_yolo-l.py --weights pretrain/Gold_l-pre-dist-state_dict_ms.ckpt
   ```