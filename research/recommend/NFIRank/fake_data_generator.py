# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the License);
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an AS IS BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
import random
import pickle
import numpy as np

from config import default_config

if __name__ == "__main__":
    train_data_path = "./data/train_data.pkl"
    test_data_path = "./data/test_data.pkl"
    config = default_config()

    train_set_nums = 100000
    train_set = {}
    history = []
    news_candidate = []
    video_candidate = []
    action = []
    reward = []
    other_info = []
    for i in range(train_set_nums):
        history.append(np.random.randint(320000, size=(10)))
        news_candidate.append(np.random.randint(320000, size=(10)))
        video_candidate.append(np.random.randint(320000, size=(10)))
        action_id = random.randint(0, 39)
        action.append(config.default_action[action_id])
        reward.append(np.random.randint(2, size=(10)))
        other_info.append(np.random.randint(10, size=(5)))

    train_set["history"] = np.array(history)
    train_set["news_candidate"] = np.array(news_candidate)
    train_set["video_candidate"] = np.array(video_candidate)
    train_set["action"] = np.array(action)
    train_set["reward"] = np.array(reward)
    train_set["other_info"] = np.array(other_info)

    f = open(train_data_path, "wb")
    pickle.dump(train_set, f)
    f.close()
