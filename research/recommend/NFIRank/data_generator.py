# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the License);
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an AS IS BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
import pickle
import numpy as np


class Dataset:
    def __init__(self, trainset_path, testset_path, candidate_actions_set):
        self.trainset_path = trainset_path
        self.testset_path = testset_path
        self.candidate_actions_set = np.array(candidate_actions_set)

        self.load_data_from_pkl()

    def load_data_from_pkl(self):
        # load data
        f_train = open(self.trainset_path, "rb")
        self.train_data = pickle.load(f_train)
        f_train.close()
        f_test = open(self.testset_path, "rb")
        self.test_data = pickle.load(f_test)
        f_test.close()
        print("data loaded.")

        # preprocess
        # train_set
        self.train_other_info = self.train_data["other_info"]
        self.train_history = self.train_data["history"]
        self.train_news_candidate = self.train_data["news_candidate"]
        self.train_video_candidate = self.train_data["video_candidate"]
        self.train_action = self.train_data["action"]
        self.train_reward = self.train_data["reward"]
        # calculate next_candidate
        self.next_news_candidate = []
        self.next_video_candidate = []
        self.video_num = []
        self.news_num = []
        self.next_video_num = []
        self.next_news_num = []
        self.train_action_mask = []
        for i in range(self.train_action.shape[0]):
            news_num = np.sum(self.train_action[i])
            video_num = 10 - np.sum(self.train_action[i])
            local_news_candidate = np.array(
                self.train_news_candidate[i][news_num:].tolist() + [-1] * news_num)
            local_video_candidate = np.array(
                self.train_video_candidate[i][video_num:].tolist() + [-1] * video_num)
            self.next_news_candidate.append(local_news_candidate)
            self.next_video_candidate.append(local_video_candidate)

            video_num_in_candidate = 10 - \
                                     np.sum(np.equal(self.train_video_candidate[i], -1))
            news_num_in_candidate = 10 - \
                                    np.sum(np.equal(self.train_news_candidate[i], -1))
            self.video_num.append(video_num_in_candidate)
            self.news_num.append(news_num_in_candidate)
            self.next_video_num.append(video_num_in_candidate - video_num)
            self.next_news_num.append(news_num_in_candidate - news_num)

            # calculate action mask
            local_action = np.array(self.train_action[i])
            local_mask = np.array(
                np.sum(self.candidate_actions_set == local_action, axis=1) == 10)
            self.train_action_mask.append(local_mask)

        self.next_news_candidate = np.array(self.next_news_candidate)
        self.next_video_candidate = np.array(self.next_video_candidate)
        self.video_num = np.array(self.video_num)
        self.news_num = np.array(self.news_num)
        self.next_video_num = np.array(self.next_video_num)
        self.next_news_num = np.array(self.next_news_num)
        self.train_action_mask = np.array(self.train_action_mask)

        # test_set
        # remove qid
        self.test_other_info = self.test_data["other_info"][:, 1:]
        self.test_history = self.test_data["history"]
        self.test_news_candidate = self.test_data["news_candidate"]
        self.test_video_candidate = self.test_data["video_candidate"]
        self.test_action = self.test_data["action"]
        self.test_reward = self.test_data["reward"]
        print("preprocess done.")

        self.train_samples_num = self.train_history.shape[0]
        self.test_samples_num = self.test_history.shape[0]
        print("train samples:", self.train_samples_num)
        print("test samples:", self.test_samples_num)

    def sample_trainset(self, batch_size=256):
        idxs = np.random.choice(self.train_samples_num,
                                batch_size, replace=False, p=None)
        return self.train_other_info[idxs, :], self.train_history[idxs, :], self.train_news_candidate[idxs, :], \
               self.train_video_candidate[idxs, :], self.train_action[idxs, :], self.train_reward[idxs, :], \
               self.next_news_candidate[idxs, :], self.next_video_candidate[idxs, :], \
               self.news_num[idxs], self.video_num[idxs], self.next_news_num[idxs], self.next_video_num[idxs], \
               self.train_action_mask[idxs]
