# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================

import numpy as np
from mindspore.nn import Metric


class MDLFMetric(Metric):
    def __init__(self, intervals, length, interval_length):
        super(MDLFMetric, self).__init__()
        self.N = intervals
        self.K = length
        self.interval_length = interval_length
        self.pdf = None
        self.cdf = None
        self.winning_rate = None
        self.pos = None
        self.score = None
        self.clear()

    def clear(self):
        self.pdf = np.zeros([0, self.K, self.N])
        self.cdf = np.zeros([0, self.K, self.N])
        self.winning_rate = np.zeros([0, self.K, self.N])
        self.score = np.zeros([0, 1], dtype=np.int)
        self.pos = np.zeros([0, 1], dtype=np.int)
        self.tot = 0
        self._anlp_pdf = 0
        self._anlp_wr = 0
        self._mae = 0
        self._cindex_global = 0
        self._cindex_local = 0

    def update(self, *inputs):
        self.pdf = inputs[0].asnumpy()
        self.cdf = inputs[1].asnumpy()
        self.winning_rate = inputs[2].asnumpy()
        lbl_1 = inputs[3].asnumpy()
        lbl_2 = inputs[4].asnumpy()
        self.pos = np.clip(lbl_1.astype(np.int), 0, self.K).reshape(-1, 1)
        self.score = np.clip((lbl_2 / self.interval_length).astype(np.int), 0, self.N - 1).reshape(-1, 1)

        self._anlp_pdf += self.eval_anlp_pdf()
        self._anlp_wr += self.eval_anlp_wr()
        self._mae += self.eval_mae()
        self._cindex_global += self.eval_cindex_global()
        self._cindex_local += self.eval_cindex_local()
        self.tot += 1

    def eval(self):
        ret = {'val_anlp_pdf': self.anlp_pdf,
                'val_anlp_wr': self.anlp_wr,
               'val_anlp_mae': self.mae,
                'val_cindex_global': self.cindex_global,
                'val_cindex_local': self.cindex_local}
        self.clear()
        return ret

    def eval_anlp_pdf(self):
        win = np.where(self.pos.reshape(-1) < self.K)
        win_pos = self.pos[win]
        win_score = self.score[win]
        if self.pdf.ndim == 2:
            win_pdf_given_pos = np.take(self.pdf, win_pos, axis=0)
        else:
            win_pdf = self.pdf[win]
            win_pdf_given_pos = np.take_along_axis(win_pdf, win_pos.reshape(-1, 1, 1), axis=1)
        win_pdf_given_pos_given_score = np.take_along_axis(win_pdf_given_pos, win_score.reshape(-1, 1, 1), axis=2)
        if win_pos.shape[0] == 0:
            return 0
        return -np.mean(np.log(win_pdf_given_pos_given_score + 1e-20))

    def eval_anlp_wr(self):
        win = np.where(self.pos.reshape(-1) < self.K)
        win_pos = self.pos[win]
        win_score = self.score[win]
        if self.winning_rate.ndim == 2:
            win_wr_given_pos = np.take(self.winning_rate, win_pos, axis=0)
        else:
            win_wr = self.winning_rate[win]
            win_wr_given_pos = np.take_along_axis(win_wr, win_pos.reshape(-1, 1, 1), axis=1)
        win_wr_given_pos_given_score = np.take_along_axis(win_wr_given_pos, win_score.reshape(-1, 1, 1), axis=2)
        if win_pos.shape[0] == 0:
            return 0
        return -np.mean(np.log(np.clip(win_wr_given_pos_given_score, 1e-20, 1 - 1e-20)))

    def eval_mae(self):
        win = np.where(self.pos.reshape(-1) < self.K)
        win_pos = self.pos[win]
        if win_pos.shape[0] == 0:
            return 0
        win_score = self.score[win].squeeze()
        if self.winning_rate.ndim == 2:
            win_wr_given_pos = np.take(self.winning_rate, win_pos, axis=0).squeeze(1)
        else:
            win_wr = self.winning_rate[win]
            win_wr_given_pos = np.take_along_axis(win_wr, win_pos.reshape(-1, 1, 1), axis=1).squeeze(1)
        predicted_score = np.argmax(win_wr_given_pos, axis=1)
        return np.mean(np.abs(predicted_score - win_score) * self.interval_length)

    def eval_cindex_global(self):
        if self.cdf.ndim == 2:
            return 1
        batch = self.pos.shape[0]
        less_score_mat = (np.reshape(self.score, [-1, 1])) < np.reshape(self.score, [1, -1])
        equal_score_mat = np.equal(np.reshape(self.score, [-1, 1]), np.reshape(self.score, [1, -1]))
        cnt = 0
        tot = 0
        for k in range(self.K):
            F_k = self.cdf[:, k, :]
            pos_k_label = self.pos <= k
            censor_pos = self.pos > k
            pos_k_F = less_score_mat | (np.repeat(np.reshape(censor_pos, [-1, 1]), batch, axis=1) & equal_score_mat)
            pos_k_F = pos_k_F & np.repeat(np.reshape(pos_k_label, [-1, 1]), batch, axis=1)
            self_F = np.take_along_axis(F_k, self.score, axis=1)
            pair_F = np.take_along_axis(np.transpose(F_k), self.score, axis=0)
            deno = np.sum(pos_k_F)
            if deno != 0:
                cnt += (np.sum((self_F > pair_F) & pos_k_F)) / np.sum(pos_k_F)
                tot += 1

        if tot == 0:
            return 1
        return cnt / tot

    def eval_cindex_local(self):
        batch = self.pos.shape[0]
        res = 0
        comparable = 0
        for i in range(batch):
            p, s = self.pos[i][0], self.score[i][0]
            if self.cdf.ndim == 2:
                F_i = self.cdf[:, s]
            else:
                F_i = self.cdf[i, :, s]
            tot = (p + 1) * (self.K - p - 1)

            if tot > 0:
                cnt = np.sum(F_i[:p + 1].reshape(-1, 1) <= F_i[p + 1:].reshape(1, -1))
                res += cnt / tot
                comparable += 1
        return res / comparable

    @property
    def anlp_pdf(self):
        if self.tot == 0:
            return self.eval_anlp_pdf()
        return self._anlp_pdf / self.tot

    @property
    def anlp_wr(self):
        if self.tot == 0:
            return self.eval_anlp_wr()
        return self._anlp_wr / self.tot

    @property
    def mae(self):
        if self.tot == 0:
            return self.eval_mae()
        return self._mae / self.tot / (self.N * self.interval_length)

    @property
    def cindex_global(self):
        if self.tot == 0:
            return self.eval_cindex_global()
        return self._cindex_global / self.tot

    @property
    def cindex_local(self):
        if self.tot == 0:
            return self.eval_cindex_local()
        return self._cindex_local / self.tot

    def summary_print(self):
        print('ANLP pdf: ', self.anlp_pdf)
        print('ANLP wr: ', self.anlp_wr)
        print('MAE: ', self.mae)
        print('C-Index Global: ', self.cindex_global)
        print('C-Index Local: ', self.cindex_local)
