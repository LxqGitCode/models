# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================

import mindspore as ms
import mindspore.nn as nn

import mindspore.ops as ops
import numpy as np


def cumtrapz(x, y):
    ret = ops.Zeros()((y.shape[0], y.shape[1] - 1), ms.float32)
    ret[:, 0] = (y[:, 0] + y[:, 1]) * (x[:, 1] - x[:, 0]) / 2
    for i in range(1, y.shape[1] - 1):
        ret[:, i] = ret[:, i - 1] + (y[:, i] + y[:, i + 1]) * (x[:, i + 1] - x[:, i]) / 2
    return ret


class DWPP(nn.Cell):
    def __init__(self, feature_num, embedding_size=8, intervals=100, length=5, interval_length=1, MAX_SPARSE=110):
        super(DWPP, self).__init__()
        self.embedding_size = embedding_size
        self.embeddings = nn.Embedding(MAX_SPARSE, self.embedding_size, embedding_table='normal')
        self.K = length
        self.N = intervals
        self.interval_length = interval_length
        self.feature_num = feature_num
        self.layers = nn.CellList()

        self.steps = ms.Tensor(np.arange(self.interval_length, (self.N + 2) * self.interval_length,
                                         self.interval_length), dtype=ms.float32).reshape(1, -1)
        self.sqrt2pi = ms.Tensor(np.sqrt(2 * np.pi), dtype=ms.float32)

        for _ in range(self.K):
            self.layers.append(nn.SequentialCell(
                nn.Dense(self.feature_num * self.embedding_size, 32),
                nn.ReLU(),
                nn.Dense(32, 2)
            ))

    def construct(self, x):
        embs = self.embeddings(x).reshape((-1, self.feature_num * self.embedding_size))
        event_output = []
        for i in range(self.K):
            ret = self.layers[i](embs)
            loc = ret[:, 0:1]
            scale = ops.exp(ret[:, 1:2])
            z = (ops.log(self.steps) - loc) / scale
            k_pdf = 1 / (self.steps * self.sqrt2pi * scale) * ops.exp(-ops.square(z) / 2)
            k_cdf = cumtrapz(self.steps, k_pdf)
            event_output.append(k_cdf)
        cdf = ops.Stack(axis=1)(event_output)
        pdf = ops.Concat(axis=2)((cdf[:, :, 0:1], cdf[:, :, 1:] - cdf[:, :, :-1]))
        winning_rate = ops.Concat(axis=1)((cdf[:, 0:1, :], cdf[:, 1:, :] - cdf[:, :-1, :]))
        return pdf, cdf, winning_rate
