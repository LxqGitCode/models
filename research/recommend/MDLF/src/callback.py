# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the License);
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# httpwww.apache.orglicensesLICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an AS IS BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""
Defined callback for MDLF.
"""
import os
import time
from mindspore.train import Callback
from mindspore import save_checkpoint


def add_write(file_path, out_str):
    with open(file_path, 'a+', encoding='utf-8') as file_out:
        file_out.write(out_str + '\n')


def res_format_str(res, loss_net=None):
    if not loss_net:
        return '\t'.join(res.tolist())
    name = loss_net.__class__.__name__
    res = res.asnumpy()
    if name == 'LFLossClass':
        ret = '    {:15s}: {}\n'.format('loss_win_f', res[0])
        ret += '    {:15s}: {}\n'.format('loss_win_w', res[1])
        ret += '    {:15s}: {}\n'.format('loss_lose_F', res[2])
        ret += '    {:15s}: {}\n'.format('loss_win_F', res[3])
        ret += '    {:15s}: {}\n'.format('tot_loss', res[4])
    elif name == 'SALossClass':
        ret = '    {:15s}: {}\n'.format('loss_1', res[0])
        ret += '    {:15s}: {}\n'.format('loss_2', res[1])
        ret += '    {:15s}: {}\n'.format('loss_rank_1', res[2])
        ret += '    {:15s}: {}\n'.format('loss_rank_2', res[3])
        ret += '    {:15s}: {}\n'.format('tot_loss', res[4])
    else:
        print(name)
        raise NotImplementedError
    return ret


def dict_format_str(dic):
    ret = ''
    for key, value in dic.items():
        ret += '    {:15s}: {}\n'.format(str(key), value)
    return ret


class EvalCallBack(Callback):
    """
    Monitor the loss in training.
    If the loss is NAN or INF terminating training.
    """

    def __init__(self, model, eval_dataset, eval_file_path, ckpt_dir, early_stop_epoch=10):
        super(EvalCallBack, self).__init__()
        self.model = model
        self.eval_dataset = eval_dataset
        self.eval_file_path = eval_file_path
        self.best_epoch = 0
        self.best_met = 10000
        self.ckpt_dir = ckpt_dir
        self.early_stop_epoch = early_stop_epoch

    def epoch_end(self, run_context):
        cb_params = run_context.original_args()
        start_time = time.time()
        out = self.model.eval(self.eval_dataset)['MDLFMetric']
        eval_time = int(time.time() - start_time)
        time_str = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
        out_str = "{}  epoch: {}  eval_time: {}s   EvalCallBack metric:\n{} ".format(
            time_str, cb_params.cur_epoch_num, eval_time, dict_format_str(out))
        print(out_str)
        add_write(self.eval_file_path, out_str)
        if out['val_anlp_pdf'] + out['val_anlp_wr'] < self.best_met:
            self.best_met = out['val_anlp_pdf'] + out['val_anlp_wr']
            self.best_epoch = cb_params['cur_epoch_num']
            save_checkpoint(cb_params.train_network, os.path.join(self.ckpt_dir, 'best.ckpt'))
        if cb_params['cur_epoch_num'] - self.best_epoch > self.early_stop_epoch:
            run_context.request_stop()


class LossCallBack(Callback):
    """
    Monitor the loss in training.
    If the loss is NAN or INF terminating training.
    Note
        If per_print_times is 0 do not print loss.
    Args
        loss_file_path (str) The file absolute path, to save as loss_file;
        per_print_times (int) Print loss every times. Default 100.
    """

    def __init__(self, loss_file_path, per_print_times=100):
        super(LossCallBack, self).__init__()
        if not isinstance(per_print_times, int) or per_print_times < 0:
            raise ValueError("print_step must be int and >= 0.")
        self.loss_file_path = loss_file_path
        self._per_print_times = per_print_times

    def step_end(self, run_context):
        """Monitor the loss in training."""
        cb_params = run_context.original_args()
        loss = cb_params.net_outputs
        cur_step_in_epoch = (cb_params.cur_step_num - 1) % cb_params.batch_num + 1
        if self._per_print_times != 0 and cur_step_in_epoch % self._per_print_times == 1:
            with open(self.loss_file_path, "a+") as loss_file:
                time_str = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
                loss_file.write("{} epoch: {} step: {}/{}, loss: \n{}".format(
                    time_str, cb_params.cur_epoch_num, cur_step_in_epoch,
                    cb_params.batch_num, res_format_str(loss, cb_params.network.network)))
            print("epoch: {} step: {}/{}, loss: \n{}".format(
                cb_params.cur_epoch_num, cur_step_in_epoch, cb_params.batch_num,
                res_format_str(loss, cb_params.network.network)))


class TimeMonitor(Callback):
    """
    Time monitor for calculating cost of each epoch.
    Args
        data_size (int) step size of an epoch.
    """

    def __init__(self):
        super(TimeMonitor, self).__init__()
        self.steps_per_epoch = 0

    def epoch_begin(self, run_context):
        self.epoch_time = time.time()

    def epoch_end(self, run_context):
        epoch_mseconds = (time.time() - self.epoch_time) * 1000
        per_step_mseconds = epoch_mseconds / self.steps_per_epoch
        self.steps_per_epoch = 0
        print("epoch time: {0}, per step time: {1}".format(epoch_mseconds, per_step_mseconds), flush=True)

    def step_begin(self, run_context):
        self.step_time = time.time()
        self.steps_per_epoch += 1

    def step_end(self, run_context):
        step_mseconds = (time.time() - self.step_time) * 1000
        cb_params = run_context.original_args()
        cur_step_in_epoch = (cb_params.cur_step_num - 1) % cb_params.batch_num + 1
        if cur_step_in_epoch % 100 == 1:
            print(f"step time {step_mseconds}\n", flush=True)
