# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
import numpy as np
import mindspore as ms
from mindspore.common.initializer import initializer
from mindspore import numpy as mnp
from mindspore import nn, ops, context
from mindspore import dataset as ds
from tqdm import tqdm

class WithLossCell(nn.Cell):
    def __init__(self, net, loss):
        super(WithLossCell, self).__init__()
        self.net = net
        self.loss = loss

    def construct(self, inputs, labels):
        logits = self.net(inputs)
        loss = self.loss(logits, labels)
        return loss

class IRT(nn.Cell):
    def __init__(self, num_students, num_questions, num_dim):
        super(IRT, self).__init__()
        self.num_dim = num_dim
        self.num_students = num_students
        self.num_questions = num_questions
        self.theta = nn.Embedding(self.num_students, self.num_dim)
        self.alpha = nn.Embedding(self.num_questions, self.num_dim)
        self.beta = nn.Embedding(self.num_questions, 1)

        for _, param in self.parameters_and_names():
            if param.dim() > 2:
                param.set_data(initializer('xavier_uniform', param.shape, param.dtype))

    def construct(self, inputs):
        student_ids, question_ids = inputs
        theta = self.theta(student_ids)
        alpha = self.alpha(question_ids)
        beta = self.beta(question_ids)
        pred = mnp.sum(alpha * theta, axis=-1, keepdims=True) + beta
        pred = ops.sigmoid(pred)
        return ops.squeeze(pred)

    def init_stu_emb(self):
        for name, param in self.parameters_and_names():
            if 'theta' in name:
                param.set_data(initializer('xavier_uniform', param.shape, param.dtype))

class IRTModel:
    def __init__(self, args, num_students, num_questions, num_dim):
        self.args = args
        self.model = IRT(num_students, num_questions, num_dim)

    @property
    def name(self):
        return 'Item Response Theory'

    def init_stu_emb(self):
        self.model.init_stu_emb()

    def train(self, train_data, lr, batch_size, epochs, path):
        device_target = context.get_context('device_target')
        print('train on ' + str(device_target))

        network = self.model
        network.set_train()
        loss_fn = nn.BCELoss(reduction='mean')
        net_opt = nn.Adam(network.trainable_params(), learning_rate=lr)
        train_loader = ds.GeneratorDataset(train_data, column_names=['sid', 'qid', 'concept_emb', 'label'],\
                                        shuffle=True).batch(batch_size)
        net_with_loss = WithLossCell(network, loss_fn)
        train_net = nn.TrainOneStepCell(net_with_loss, net_opt)

        best_loss = 1000000
        for _ in range(1, epochs + 1):
            loss = 0.0
            tqdm_obj = tqdm(train_loader.create_dict_iterator(), total=len(train_data) // batch_size)
            for _, batch in enumerate(tqdm_obj):
                bz_loss = train_net((batch['sid'], batch['qid']), batch['label'])
                loss += bz_loss.asnumpy()
            loss /= (len(train_data) // batch_size)
            print('loss=' + str(loss))
            if loss < best_loss:
                best_loss = loss
                print('Store model')
                self.adaptest_save(path)

    def adaptest_save(self, path):
        model_dict = self.model.parameters_dict()
        model_list = [{'name': k, 'data': v} for k, v in model_dict.items()]
        ms.save_checkpoint(model_list, path)

    def adaptest_load(self, path):
        param_dict = ms.load_checkpoint(path, filter_prefix='net.theta')
        _, _ = ms.load_param_into_net(self.model, param_dict, strict_load=False)

    def cal_loss(self, sids, query_rates, know_map):
        real, pred = [], []
        loss_fn = nn.BCELoss(reduction='mean')
        all_loss = np.zeros(len(sids))
        self.model.set_grad(False)
        self.model.set_train(False)
        for idx, sid in enumerate(sids):
            question_ids = list(query_rates[sid].keys())
            student_ids = [sid] * len(question_ids)
            labels = [query_rates[sid][qid] for qid in question_ids]
            real.append(np.array(labels))
            student_ids = ms.Tensor(student_ids)
            question_ids = ms.Tensor(question_ids)
            labels = ms.Tensor(labels, dtype=ms.float32)
            inputs = (student_ids, question_ids)
            logits = self.model(inputs)
            loss = loss_fn(logits, labels)
            all_loss[idx] = loss.asnumpy()
            pred.append(logits.asnumpy())
        self.model.set_grad()
        self.model.set_train()
        return all_loss, pred, real

    def update(self, tested_dataset, lr, epochs, batch_size):
        network = self.model
        network.set_train()
        network.set_grad()
        loss_fn = nn.BCELoss(reduction='mean')
        net_opt = nn.Adam(network.theta.trainable_params(), learning_rate=lr)
        dataloader = ds.GeneratorDataset(tested_dataset, column_names=['sid', 'qid', 'concept_emb', 'label'],\
                                        shuffle=True).batch(batch_size)
        net_with_loss = WithLossCell(network, loss_fn)
        train_net = nn.TrainOneStepCell(net_with_loss, net_opt)
        for _ in range(1, epochs + 1):
            loss = 0.0
            for _, batch in enumerate(dataloader.create_dict_iterator()):
                bz_loss = train_net((batch['sid'], batch['qid']), batch['label'])
                loss += bz_loss.asnumpy()
