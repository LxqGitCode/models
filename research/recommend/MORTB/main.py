# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the License);
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an AS IS BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""
Bidding simulation.
"""
import os
import argparse
import math

from scipy import optimize as op
import numpy as np
import pandas as pd
from src.click_models import PositionBiasedModel
from src.landscape import Landscape

SCORE = ['p1_ecpm', 'p2_ecpm', 'p3_ecpm', 'p4_ecpm', 'p5_ecpm', 'p6_ecpm', 'p7_ecpm', 'p8_ecpm', 'p9_ecpm', 'p10_ecpm']
CAMPAIGN = ['10341182', '30801593', '17686799', '15398570', '5061834', '15184511', '29427842', '28351001', '18975823',
            '31772643']


# == functions for S_ORTB  ==
def s_ortb(c, base, r):
    t2 = (c / r) ** 2 * (math.sqrt((c / r) ** 2 + base * base))
    t1 = base * (c / r) ** 2
    return math.pow(t1 + t2, 1 / 3) - math.pow(t2 - t1, 1 / 3)


def win_rate(x, c):
    return x * x / (c * c + x * x)


def fit_win_rate(info, length):
    data = np.array(info[SCORE[:length]]).reshape(-1)
    counts, bins = np.histogram(data, 200, range=(0, 0.03), normed=True, density=True)
    wins = np.cumsum(counts) * (bins[1] - bins[0])
    return op.curve_fit(win_rate, bins[:-1], wins)[0]


# =========


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--data_path', default='data_ecpm_full/', type=str, help='Directory for the dataset')
    parser.add_argument('--output_path', default='Logs/', type=str, help='Directory for the dataset')
    parser.add_argument('--method', default='Linear', choices=['MKB', 'Linear', 'S_ORTB', 'M_ORTB'],
                        type=str, help='bidding strategy')
    parser.add_argument('--mode', default='search', choices=['test', 'search'], type=str,
                        help='\'test\' for a day test, \'search\' for searching the optimal lambda')
    parser.add_argument('--start_day', default=1, type=int, help='start day of the experiment')
    parser.add_argument('--end_day', default=30, type=int, help='end day of the experiment')
    parser.add_argument('--length', default=5, type=int, help='the length of the list')
    parser.add_argument('--campaign', default='10341182',
                        choices=['10341182', '30801593', '17686799', '15398570', '5061834', '15184511', '29427842',
                                 '28351001', '18975823', '31772643'],
                        type=str, help='campaign id')
    parser.add_argument('--uk', default=[0.68, 0.61, 0.48, 0.34, 0.28, 0.20, 0.11, 0.10, 0.08, 0.06],
                        nargs='+', type=float)
    parser.add_argument('--budget', default=2, type=float, help='budget, the proportion of the original cost')
    parser.add_argument('--lda', default=1, type=float, help='assign a lambda for test')
    parser.add_argument('--gamma', default=1, type=float, help='decay on the exponential')
    parser.add_argument('--comment', default='', type=str, help='Other comments to be attached')
    FLAGS, _ = parser.parse_known_args()
    return FLAGS


def test_day(df, lda, method, mode='test', length=10, **kwargs):
    if mode == 'search':
        landscape = kwargs['landscape']
        click_model = kwargs['click_model']
        budget = 10000
    else:
        u_k = kwargs['uk']
        gamma = kwargs['gamma']
        landscape = Landscape(df, u_k=u_k, gamma=gamma, len_list=length)
        click_model = PositionBiasedModel(neg_click_prob=0.1, origin_exam_prob=u_k, eta=gamma)
        budget = kwargs['budget']

    # record the cost list
    cost_list = [0] * 24

    ctrs = np.array(df['ctr'])
    cvrs = np.array(df['cvr'])
    bids = np.array(df['bid'])
    cpos = np.array(df['cpo'])
    hour = (np.floor(df['timestamp'] / 3600).astype(int)) % 24
    scores = np.array(df[SCORE])

    clks = np.array(df['click'])

    n = len(df)
    sample_cost = 0
    sample_gain_value = 0
    exp_cost = 0
    exp_gain_value = 0
    exp_clk = 0
    exp_conv = 0
    win = 0
    it_time = 0
    wins = np.zeros(length + 1)

    # for controlling
    cost_hour = 0
    cur_hour = 0

    for i in range(n):
        if hour[i] != cur_hour:
            cost_list[cur_hour] = cost_hour
            cur_hour = hour[i]
            cost_hour = 0

        base = bids[i] / lda
        if method in ('MKB', 'Linear'):
            bid = base
        elif method == 'S_ORTB':
            bid = s_ortb(kwargs['c'], base, ctrs[i])
        elif method == 'M_ORTB':
            bid, it = landscape.getNewtonBid(bids[i], ctrs[i], lda)
            it_time += it
        else:
            print('Specify a bid opt method!')
            raise ValueError
        # get pos:
        s = bid * ctrs[i]
        pos = 0
        while pos < length and s < scores[i][pos]:
            pos += 1
        wins[pos] += 1
        if pos >= length:
            continue
        win += 1
        sample_click, exam_prob, rel_prob = click_model.sampleClick(pos, clks[i])

        if sample_click:
            sample_cost += bid
            sample_gain_value += cpos[i] * cvrs[i]
        exp_cost += bid * exam_prob * rel_prob
        exp_gain_value += cpos[i] * cvrs[i] * exam_prob * rel_prob
        exp_clk += exam_prob * rel_prob
        exp_conv += cvrs[i] * exam_prob * rel_prob
        cost_hour += bid * exam_prob * rel_prob
        if exp_cost > budget:
            break
    cost_list[cur_hour] = cost_hour

    if method == 'M_ORTB':
        print('Average iteration steps: ' + str(it_time / n))

    return [sample_cost, sample_gain_value, exp_cost, exp_gain_value, exp_clk, exp_conv], cost_list


def solve_lambda(df, method, length=10, **kwargs):
    u_k = kwargs['uk']
    gamma = kwargs['gamma']
    landscape = Landscape(df, u_k=u_k, gamma=gamma, len_list=length)
    click_model = PositionBiasedModel(neg_click_prob=0.1, origin_exam_prob=u_k, eta=gamma)
    l_lda, r_lda = 0.0000001, 20
    kwargs['landscape'] = landscape
    kwargs['click_model'] = click_model
    budget = kwargs['budget']
    while True:
        lda = (l_lda + r_lda) / 2
        [_, _, exp_cost, _, _, _], cost_list = test_day(df, lda, method, 'search', length=length, **kwargs)

        print(lda, exp_cost, budget)

        if exp_cost > budget:
            l_lda = lda
        else:
            r_lda = lda
        if abs(exp_cost - budget) / budget < 0.005 or abs(r_lda - l_lda) < 1e-6:
            return lda, cost_list


def transform_keyword_bid(df):
    bucket = (0.8 * max(df['bid']) - min(df['bid'])) / 10
    df['bid_group'] = (df['bid'] / bucket).astype(int).clip(lower=0, upper=9)
    return df.join(df[['bid_group', 'bid']].groupby('bid_group').mean(), on='bid_group', lsuffix='_imp')


if __name__ == '__main__':
    args = parse_args()
    print('Mode: %s' % args.mode)
    print('Budget: %f' % args.budget)
    print('Method: %s' % args.method)
    print('Campaign: %s' % args.campaign)
    print('Start Day: %d' % args.start_day)
    print('End Day: %d' % args.end_day)
    print('Length: %d' % args.length)
    print('Gamma: %f' % args.gamma)
    print(args.comment)

    kwargs = {'uk': args.uk, 'gamma': args.gamma}
    if not os.path.exists(args.output_path):
        os.mkdir(args.output_path)
    if not os.path.exists(os.path.join(args.output_path, args.campaign)):
        os.mkdir(os.path.join(args.output_path, args.campaign))
    f = open(os.path.join(args.output_path, args.campaign, args.method + '_'
                          + str(args.budget) + args.comment + '.csv'), 'w')
    f.write('Day,Budget,Cost,Value,Click,Conversion\n')
    for day in range(args.start_day, args.end_day):
        DATA_FILE = os.path.join(args.data_path, args.campaign, str(day))
        NEXT_DATA_FILE = os.path.join(args.data_path, args.campaign, str(day + 1))
        data = pd.read_csv(DATA_FILE, sep='\t')
        next_data = pd.read_csv(NEXT_DATA_FILE, sep='\t')

        if args.method == 'S_ORTB':
            param_c = fit_win_rate(data, args.length)[0]
            print('Fitted parameter for S_ORTB: ', param_c)
            kwargs['c'] = param_c

        kwargs['budget'] = 100000
        [_, _, MAX_COST, _, _, _], _ = test_day(data, 1, 'Linear', length=args.length, **kwargs)
        kwargs['budget'] = args.budget * MAX_COST
        if 'MKB' in args.method:
            data = transform_keyword_bid(data)
            next_data = transform_keyword_bid(next_data)

        if args.mode == 'search':
            cur_lda, _ = solve_lambda(data, args.method, length=args.length, **kwargs)
        else:
            cur_lda = args.lda

        # for test
        [test_sample_cost, test_sample_gain_value, test_exp_cost, test_exp_gain_value, test_exp_clk, test_exp_conv], \
            test_cost_list = test_day(next_data, cur_lda, args.method, 'test', length=args.length, **kwargs)
        print('===== Summary =====')
        print(day + 1, cur_lda, test_sample_cost, test_sample_gain_value,
              test_exp_cost, test_exp_gain_value, test_exp_clk, test_exp_conv)
        print('Budget: %f, Cost: %f, Value: %f, ROI: %f, CPA: %f' %
              (kwargs['budget'], test_exp_cost, test_exp_gain_value,
               test_exp_gain_value / (test_exp_cost + 0.0000000001), test_exp_cost / test_exp_conv))
        print('Cost by hour: ', test_cost_list)
        print('==========\n')

        f.write('%d,%f,%f,%f,%f,%f\n' % (day, kwargs['budget'], test_exp_cost,
                                         test_exp_gain_value, test_exp_clk, test_exp_conv))
    f.close()
