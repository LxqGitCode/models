import h5py

def to_hdf(na, path, key):
    print(key, na.shape)
    with h5py.File(str(path), 'a') as hf:
        hf.create_dataset(key, data=na)

def from_hdf(path, key, start=0, stop='last'):
    with h5py.File(str(path), 'r') as hf:
        if stop == 'last':
            data = hf[key][start:]
        else:
            data = hf[key][start:stop]
    return data
