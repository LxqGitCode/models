# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
from mindspore import nn
from .basic_cell import BasicCell


class PitchEncoder(BasicCell):
    def __init__(self, use_encoder=True, latent_dim=300):
        super(PitchEncoder, self).__init__()
        self.use_encoder = use_encoder
        self.latent = latent_dim
        self.pitch_fc = None
        self.pitch_norm = None
        if self.use_encoder:
            self.pitch_fc = nn.Dense(400, latent_dim)
            self.pitch_norm = nn.LayerNorm([latent_dim])

    def construct(self, f0):
        if self.use_encoder:
            f0 = self.pitch_fc(f0)
            f0 = self.pitch_norm(f0)
        return f0


class SpeakerEncoder(BasicCell):
    def __init__(
        self,
        use_encoder=True,
        latent_dim=300,
        use_spk_pseudo_label=True,
        spk_class_num=100,
    ):
        super(SpeakerEncoder, self).__init__()
        self.use_encoder = use_encoder
        self.latent_dim = latent_dim
        self.spk_fc = None
        self.spk_norm = None
        self.use_spk_pseudo_label = use_spk_pseudo_label
        self.spk_class_num = spk_class_num
        if self.use_encoder:
            self.spk_fc = nn.Dense(256, latent_dim)
            self.spk_norm = nn.LayerNorm([latent_dim])
        if self.use_spk_pseudo_label:
            self.classifier = nn.Dense(latent_dim, spk_class_num)

    def construct(self, x):
        out = x
        if self.use_encoder:
            out = self.spk_fc(out)
            out = self.spk_norm(out)
        if self.use_spk_pseudo_label:
            score = self.classifier(out)
            return score, out
        return out
