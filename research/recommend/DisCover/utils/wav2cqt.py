# MIT License
#
# Copyright (c) 2020 zhesong yu
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
'''
Modified from: https://github.com/yzspku/CQTNet
'''
# Generate CQT from wavefile
# Save as npy
import os
from multiprocessing import Pool
import librosa
import numpy as np
from tqdm import tqdm

IN_DIR = ""
OUT_DIR = ""


def cqt_extractor(args):
    _in_path, _out_path = args
    data, sr = librosa.load(_in_path, sr=22050)
    if len(data) < 1000:
        return
    cqt = np.abs(librosa.cqt(y=data, sr=sr))
    mean_size = 20
    height, length = cqt.shape
    new_cqt = np.zeros((height, int(length / mean_size)), dtype=np.float64)
    for ep in range(int(length / mean_size)):
        new_cqt[:, ep] = cqt[:, ep * mean_size : (ep + 1) * mean_size].mean(axis=1)
    np.save(_out_path, new_cqt)

if __name__ == "__main__":
    params = []
    for i, (root, dirs, files) in tqdm(enumerate(os.walk(IN_DIR))):
        files_num = len(files)
        if files_num > 0:
            for file in files:
                in_path = os.path.join(root, file)
                set_id = root.split("/")[-1]
                out_path = OUT_DIR + file.split(".")[0] + ".npy"
                params.append((in_path, out_path))

    print("begin")
    pool = Pool(40)
    pool.map(cqt_extractor, params)
    pool.close()
    pool.join()
    print("end")
