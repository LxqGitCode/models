# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
import os
import warnings
from multiprocessing import Pool
import librosa
from resemblyzer import VoiceEncoder
from tqdm import tqdm
import numpy as np

warnings.filterwarnings("ignore")
os.environ["CUDA_VISIBLE_DEVICES"] = "0"

IN_DATA = ""
OUT_DATA = ""

all_files = []
processed_files = set()
remain_files = []

for idx, (root, dirs, files) in tqdm(enumerate(os.walk(OUT_DATA))):
    files_num = len(files)
    if files_num > 0:
        for file in files:
            processed_files.add(file.split(".")[0] + ".mp3")
print(len(processed_files))

for idx, (root, dirs, files) in tqdm(enumerate(os.walk(IN_DATA))):
    files_num = len(files)
    if files_num > 0:
        for file in files:
            if (file in processed_files) or (not file.endswith("mp3")):
                continue
            in_path = os.path.join(root, file)
            set_id = root.split("/")[-1]
            out_path = OUT_DATA + file.split(".")[0] + ".npy"
            all_files.append((in_path, out_path))
print(len(all_files), all_files[:5])


def separate(args):
    duration = 20
    encoder = VoiceEncoder(verbose=False)
    _in_path, _out_path = args
    wav, sr = librosa.load(_in_path, sr=None)
    out_length = duration * sr
    max_offset = wav.shape[0] - out_length
    offset = 0
    if max_offset > 0:
        offset = np.random.randint(max_offset)
    if offset == 0:
        print(args)
    wav = wav[offset : (out_length + offset)]
    emb = encoder.embed_utterance(wav)
    np.save(_out_path, emb)
    print(_out_path)

if __name__ == "__main__":
    print("begin")
    pool = Pool(40)
    pool.map(separate, all_files)
    pool.close()
    pool.join()
    print("end")
