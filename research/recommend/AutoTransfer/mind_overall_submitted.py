# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except StopIteration as e in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================

import os
import stat
import time
import numpy as np
from tqdm import tqdm
from sklearn.metrics import roc_auc_score, log_loss
import mindspore as ms
from mindspore import nn, ops
from selected_data.mind_data_process import Selecteddataset, Crossdataset, save_selected_data, clear_selected_data
from torchfm.mind_layer import Featuresembedding, Mlp, kmax_pooling
from torchfm.model.mind_afm import Afm

ms.set_context(device_target="CPU", device_id=0)


def get_dataset(name, path):
    if name == 'amazon':
        dataset = Selecteddataset(path, name='amazon')
    elif name == 'ml25m':
        dataset = Selecteddataset(path, name='ml25m')
    else:
        raise ValueError('unknown dataset name: ' + name)
    return dataset


def get_model(name, field_dims):
    if name == 'afm':
        return Afm(field_dims, embed_dim=16, attn_size=16, dropouts=(0.2, 0.2))
    raise ValueError('unknown model name: ' + name)


class Controllernetwork(nn.Cell):
    def __init__(self, field_dims, embed_dim, mlp_dims, dropout):
        super(Controllernetwork, self).__init__()
        self.embedding = Featuresembedding(field_dims, embed_dim)
        self.embed_output_dim = len(field_dims) * embed_dim
        self.mlp = Mlp(self.embed_output_dim, mlp_dims, dropout)
        self.controller_losses = None

    def construct(self, x):
        embed_x = self.embedding(x).view((-1, self.embed_output_dim))
        output_layer = self.mlp(embed_x)
        return output_layer


class Earlystopper():
    def __init__(self, num_trials, save_path):
        self.num_trials = num_trials
        self.trial_counter = 0
        self.best_auc = 0
        self.logloss = 0.0
        self.save_path = save_path

    def is_continuable(self, model, accuracy, logloss):
        if accuracy > self.best_auc:
            self.best_auc = accuracy
            self.logloss = logloss
            self.trial_counter = 0
            ms.save_checkpoint(model, self.save_path)
            return True
        if self.trial_counter + 1 < self.num_trials:
            self.trial_counter += 1
            return True
        return False


def train_target_one_epoch(model, optimizer, data_loader, criterion):
    model.set_train(True)

    def forward_fn(data, label):
        logits = model(data)
        loss = criterion(logits, label.float()).mean()
        return loss, logits

    grad_fn = ms.value_and_grad(forward_fn, None, optimizer.parameters, has_aux=True)

    def train_step(data, label):
        (loss, _), grads = grad_fn(data, label)
        optimizer(grads)
        return loss

    for fields, label, _ in tqdm(data_loader.create_tuple_iterator(num_epochs=1)):
        train_step(fields, label)


def test_all(model, data_loader, overlap_user=None):
    model.set_train(False)
    labels, predicts = [], []
    if overlap_user is not None:
        for fields, label in data_loader:
            index = []
            noindex = []
            for j in range(fields.shape[0]):
                if fields[j, 0] in overlap_user:
                    index.append(j)
                else:
                    noindex.append(j)
            if index:
                fields_overlap = fields[index]
                label_overlap = label[index]
                y_o = model(fields_overlap, overlap=True)
                labels.extend(label_overlap.asnumpy().tolist())
                predicts.extend(y_o.asnumpy().tolist())
            if noindex:
                fields_no = fields[noindex]
                label_no = label[noindex]
                y_no = model(fields_no)
                labels.extend(label_no.asnumpy().tolist())
                predicts.extend(y_no.asnumpy().tolist())
        return roc_auc_score(labels, predicts), log_loss(labels, predicts)
    for fields, label in data_loader:
        y = model(fields)
        labels.extend(label.asnumpy().tolist())
        predicts.extend(y.asnumpy().tolist())
    return roc_auc_score(labels, predicts), log_loss(labels, predicts)


def test_a_batch(model, fields, label):
    model.set_train(False)
    predicts = model(fields).asnumpy().tolist()
    labels = label.asnumpy().tolist()
    return roc_auc_score(labels, predicts), log_loss(labels, predicts)


def train_nofullbatch(model_name, field_dims, learning_rate, weight_decay, valid_data_loader, controller,
                      optimizer_controller, data_loader, len_loader, criterion, controllerLoss, epsilon):
    threshold = ms.Tensor(0.5)
    epsilon = ms.Tensor(epsilon)
    valid_iter = valid_data_loader.create_tuple_iterator(num_epochs=1)
    model = get_model(model_name, field_dims)
    optimizer = nn.Adam(
        params=model.trainable_params(), learning_rate=learning_rate, weight_decay=weight_decay)

    def forward_model(data, label):
        pred = model(data)
        loss = criterion(pred, label.float()).mean()
        return loss, pred
    grad_model = ms.value_and_grad(forward_model, None, optimizer.parameters, has_aux=True)

    def forward_controller(data, label, reward):
        pred = controller(data).squeeze()
        loss = ops.sum(controllerLoss(pred, label) * reward)
        return loss, pred
    grad_controller = ms.value_and_grad(forward_controller, None, optimizer_controller.parameters, has_aux=True)

    for fields, label, cross in tqdm(data_loader.create_tuple_iterator(num_epochs=1), total=len_loader):
        model.set_train(False)
        controller.set_train(False)
        has_target = True
        has_source = True
        label = label.reshape((-1, 1))
        target_idx = ops.nonzero(cross).squeeze()
        if target_idx is None or target_idx.ndimension() == 0 or target_idx.shape[0] == 0:
            has_target = False
            print("Target is none.")
        else:
            target_idx = target_idx.reshape((-1, 1))
            target_fields = ops.gather_elements(
                fields, 0, target_idx.tile((1, fields.shape[1])))
            target_label = (ops.gather_elements(
                label, 0, target_idx.tile((1, label.shape[1])))).squeeze()
        source_idx = ops.nonzero(1 - cross).squeeze()
        if source_idx is None or source_idx.ndimension() == 0 or source_idx.shape[0] == 0:
            has_source = False
            print("Source is none.")
        else:
            source_idx = source_idx.reshape((-1, 1))
            source_fields = ops.gather_elements(
                fields, 0, source_idx.tile((1, fields.shape[1])))
            source_label = ops.gather_elements(
                label, 0, source_idx.tile((1, label.shape[1]))).squeeze()
            y = model(source_fields)
            loss_list = criterion(y, source_label.float())
        try:
            test_fields, test_label = next(iter(valid_iter))
        except StopIteration:
            valid_iter = valid_data_loader.create_tuple_iterator(num_epochs=1)
            test_fields, test_label = next(iter(valid_iter))
        auc, _ = test_a_batch(model, test_fields, test_label)
        controller.set_train(True)
        output_layer = controller(source_fields)[:, 1].squeeze()
        prob_instance = ops.softmax(output_layer)
        sampled_actions = ops.where(prob_instance > threshold, ms.Tensor(1), ms.Tensor(0))
        random_action = ops.rand(sampled_actions.shape)
        sampled_actions1 = ops.where(random_action >= epsilon, ms.Tensor(0), ms.Tensor(1))
        sampled_actions = ((sampled_actions+sampled_actions1) % 2)
        prob_idx = ops.nonzero(sampled_actions).squeeze()
        if prob_idx is None or prob_idx.ndimension() == 0 or prob_idx.shape[0] == 0:
            print("No instance sampled.")
            continue
        selected_label = ops.gather_elements(source_label, 0, prob_idx)
        selected_instance = ops.gather_elements(
            source_fields, 0, prob_idx.reshape((-1, 1)).tile((1, source_fields.shape[1])))
        model.set_train(True)
        if not has_target:
            (_, _), grads = grad_model(selected_instance, selected_label)
        elif not has_source:
            (_, _), grads = grad_model(target_fields, target_label)
        else:
            fields = ops.cat((target_fields, selected_instance))
            label = ops.cat((target_label, selected_label))
            (_, y), grads = grad_model(fields, label)
        optimizer(grads)
        model.set_train(False)
        if has_source:
            auc1, _ = test_a_batch(model, test_fields, test_label)
            y = model(source_fields)
            loss_list1 = criterion(y, source_label.float()).float()
            reward = ms.Tensor(ms.Tensor(auc1-auc) * (loss_list - loss_list1), dtype=ms.float32)
            sampled_actions = ms.Tensor(sampled_actions, dtype=ms.int32)
            (_, _), grads = grad_controller(source_fields, sampled_actions, reward)
            optimizer_controller(grads)


def train_target(field_dims, train_data_loader, valid_data_loader, test_data_loader,
             model_name, learning_rate, criterion, weight_decay, save_rs_name):
    model = get_model(model_name, field_dims)
    optimizer = nn.Adam(
        params=model.trainable_params(), learning_rate=learning_rate, weight_decay=weight_decay)
    early_stopper = Earlystopper(
        num_trials=3, save_path=save_rs_name)
    train_start_time = time.time()
    for epoch_i in range(40):
        train_target_one_epoch(model, optimizer, train_data_loader, criterion)
        auc, logloss = test_all(model, valid_data_loader)
        print('\tepoch:', epoch_i,
              'validation: auc:', auc, 'logloss:', logloss)
        if not early_stopper.is_continuable(model, auc, logloss):
            print(f'\tvalidation: best auc: {early_stopper.best_auc}')
            break
    train_end_time = time.time()
    print("\tTime of target training: {:.2f}min".format(
        (train_end_time - train_start_time)/60))
    ms.load_param_into_net(model, ms.load_checkpoint(save_rs_name))
    model.set_train(False)
    if test_data_loader is not None:
        auc, logloss = test_all(model, test_data_loader)
        print(f'\ttest auc: {auc}, logloss: {logloss}\n')
        return auc, logloss, model
    return early_stopper.best_auc, early_stopper.logloss, model


def select_instance(data_set, selected_data_path, controller, select_ratio):
    clear_selected_data(selected_data_path)
    slct_number = int(select_ratio*len(data_set))
    fields, label = ms.Tensor.from_numpy(data_set.field), ms.Tensor.from_numpy(data_set.label)
    controller.set_train(False)
    output_layer = controller(fields)
    prob_instance = ops.softmax(output_layer)
    prob_idx = kmax_pooling(prob_instance[:, 1], 0, slct_number)
    if prob_idx is not None and prob_idx.dim != 0:
        if prob_idx.shape[0] != 0:
            selected_label = ops.gather_elements(label, 0, prob_idx)
            selected_instance = ops.gather_elements(
                fields, 0, prob_idx.unsqueeze(1).tile((1, fields.shape[1])))
            save_selected_data(selected_data_path, selected_instance.asnumpy(), selected_label.asnumpy())
        else:
            print("No sample selected!")
    else:
        print("No sample selected!")
    print("---------------------------------Select_ratio: ", select_ratio)
    print("---------------------------------Select: ", len(selected_label))
    return slct_number


def dataloader(dataset, batch_size, shuffle=True):
    if shuffle:
        loader = dataset.to_generator().shuffle(len(dataset))
    else:
        loader = dataset.to_generator()
    return loader.batch(batch_size=batch_size)


def main(dataset_setting,
         model_name,
         epoch,
         ratio_num,
         select_start,
         select_end,
         learning_rate,
         batch_size,
         weight_decay,
         save_dir,
         repeat_experiments,
         select_ratio,
         epsilon):
    if dataset_setting == "amazon":
        trans_name = 'Automotive'
        dataset_name = 'Industrial_and_Scientific'
    elif dataset_setting > "ml25m":
        trans_name = 'ml-25m_11'
        dataset_name = 'ml-25m_10'
    else:
        raise ValueError('unknown dataset setting: ' + dataset_setting)
    print('Dataset setting:', dataset_setting)
    path_source = './data/' + trans_name + '/' + trans_name + '.inter'
    path_target = './data/' + dataset_name + '/' + dataset_name + '.inter'
    print("-source dataset:", path_source)
    print("-target dataset:", path_target)
    print('Save dir:', save_dir)
    print('Model name:', model_name)
    print('Epoch:', epoch)
    print('Learning rate:', learning_rate)
    print('Batch size:', batch_size)
    print('Weight decay:', weight_decay)
    print('Device:', device)
    print('Select ratio:', select_ratio)
    print('Epsilon:', epsilon)
    print('Ratio num:', ratio_num)
    print('Select start:', select_start)
    print('Select end:', select_end)

    dataset_trans = get_dataset(dataset_setting, path_source)
    dataset_target = get_dataset(dataset_setting, path_target)
    dataset_train, dataset_valid, dataset_test = dataset_target.split()

    info = '{}_{}_{}_{}_{}'.format(model_name, dataset_setting, str(epoch), str(batch_size), str(learning_rate))
    save_controller_name = './{}/controller_whole_'.format(
        save_dir) + info + '.ckpt'
    save_rs_name = save_controller_name.replace('controller', 'rs')
    selected_data_path = './selected_data/notFixed_whole_{}_{}_train.txt'.format(
        model_name, dataset_setting)
    criterion = nn.BCELoss(reduction='none')
    dataset_cross = Crossdataset(dataset_train, dataset_trans)
    dataset_train = Crossdataset(dataset_train, None)
    train_data_loader = dataloader(dataset_train, batch_size, shuffle=True)
    cross_data_loader = dataloader(dataset_cross, batch_size, shuffle=True)
    valid_data_loader = dataloader(dataset_valid, batch_size, shuffle=True)
    test_data_loader = dataloader(dataset_test, batch_size, shuffle=True)
    field_dims = []
    for idx in range(len(dataset_train.field_dims)):
        field_dims.append(int(max(dataset_cross.field_dims[idx], dataset_valid.field_dims[idx],
                              dataset_test.field_dims[idx])))
    controller = Controllernetwork(
        field_dims, embed_dim=16, mlp_dims=(64, 64), dropout=0.2)
    optimizer_controller = nn.Adam(
        params=controller.trainable_params(), learning_rate=learning_rate * 0.001, weight_decay=weight_decay)
    controllerLoss = nn.CrossEntropyLoss(reduction='none')
    print('\n****************************** Searching Phase ******************************\n')
    trans_num = len(dataset_trans)
    for epoch_i in range(epoch):
        print("\n****************************** Epoch: {} *********************************".format(epoch_i))
        train_nofullbatch(model_name, field_dims, learning_rate, weight_decay, valid_data_loader, controller,
                          optimizer_controller, cross_data_loader, len(dataset_cross),
                          criterion, controllerLoss, epsilon)
    ms.save_checkpoint(controller, save_controller_name)
    print(
        '\n\t========================= Target Training Phase ==========================\n')
    print('\t start selection...')
    max_auc = 0.0
    max_logloss = 1000000.0
    test_auc = 0
    test_logloss = 0
    max_ratio = 0.0
    max_slct = 0
    auc_list = [0]
    logloss_list = [0]
    x_list = [0.0]
    if select_ratio is None:
        for select_ra in np.linspace(select_start, select_end, num=ratio_num):
            x_list.append(select_ra)
            slct_number = select_instance(dataset_trans, selected_data_path, controller, select_ra)
            dataset_select = Selecteddataset(selected_data_path, name="selected")
            print("\tRatio: {}\tSelect number: [{} / {}]\tSelect path: {}".format(select_ra, slct_number, trans_num,
                                                                       selected_data_path))
            dataset_merge = Crossdataset(dataset_train, dataset_select)
            selected_data_loader = dataloader(dataset_merge, batch_size, shuffle=True)
            retrain_auc, retrain_logloss, model = train_target(field_dims, selected_data_loader, valid_data_loader,
                                                               None, model_name, learning_rate, criterion, weight_decay,
                                                               save_rs_name)
            auc, logloss = test_all(model, test_data_loader)
            auc_list.append(auc)
            logloss_list.append(logloss)
            if retrain_auc > max_auc:
                max_ratio = select_ra
                max_auc = retrain_auc
                max_logloss = retrain_logloss
                max_slct = slct_number
                test_auc = auc
                test_logloss = logloss
    else:
        x_list.append(select_ratio)
        slct_number = select_instance(dataset_trans, selected_data_path, controller, select_ratio)
        dataset_select = Selecteddataset(selected_data_path, name="selected")
        print("\tRatio: {}\tSelect number: [{} / {}]\tSelect path: {}".format(select_ratio, slct_number, trans_num,
                                                                              selected_data_path))
        dataset_merge = Crossdataset(dataset_train, dataset_select)
        selected_data_loader = dataloader(dataset_merge, batch_size=batch_size, shuffle=True)
        retrain_auc, retrain_logloss, model = train_target(field_dims, selected_data_loader, valid_data_loader,
                                                           None, model_name, learning_rate, criterion, weight_decay,
                                                           save_rs_name)
        auc, logloss = test_all(model, test_data_loader, device)
        auc_list.append(auc)
        logloss_list.append(logloss)
        if retrain_auc > max_auc:
            max_ratio = select_ratio
            max_auc = retrain_auc
            max_logloss = retrain_logloss
            max_slct = slct_number
            test_auc = auc
            test_logloss = logloss
    print("\tTesting Target...")
    auc_list[0], logloss_list[0], _ = train_target(field_dims, train_data_loader, valid_data_loader,
                                                   test_data_loader,
                                                   model_name, learning_rate, criterion, weight_decay, save_rs_name)
    print("\tTesting Merge...")
    dataset_merge = Crossdataset(dataset_train, dataset_trans)
    merge_data_loader = dataloader(dataset_merge, batch_size=batch_size, shuffle=True)
    auc_merge, logloss_merge, _ = train_target(field_dims, merge_data_loader, valid_data_loader,
                                               test_data_loader,
                                               model_name, learning_rate, criterion, weight_decay, save_rs_name)
    auc_list.append(auc_merge)
    logloss_list.append(logloss_merge)
    x_list.append(1.0)
    print('\t========================= End Training and Testing =========================\n')
    print("\t----------------------------------------------------------------------------\n")
    print("Target: auc_{:.8f}  logloss_{:.8f}".format(
        auc_list[0], logloss_list[0]))
    print("Merge: auc_{:.8f}  logloss_{:.8f}".format(
        auc_list[-1], logloss_list[-1]))
    print("Selection: auc_{:.8f}  logloss_{:.8f}".format(
        test_auc, test_logloss))
    print("Best Ratio: {}, Slct: [{} / {}]".format(max_ratio, max_slct, trans_num))
    final_performance = [max_auc, max_logloss]
    flags = os.O_RDWR | os.O_CREAT
    modes = stat.S_IWUSR | stat.S_IRUSR
    with os.fdopen(os.open('Record_data/%s_%s_notFixed_whole.txt' % (model_name, dataset_setting), flags, modes),
                   'w') as fout:
        fout.write(
            '\nModel:%s\nDataset:%s\nSearching Epochs: %d\nLearning Rate: %s\nBatch Size: %d\nWeight Decay: \
            %s\nDevice: %s\nSelect Ratio: %s\nEpsilon: %s\nRatio Num: %d\nSelect start: \
            %s\nSelect End: %s\nFinal performance: %s\n'
            % (model_name, dataset_setting, epoch_i + 1, str(learning_rate), batch_size, str(weight_decay), device,
               str(max_ratio), str(epsilon), ratio_num, str(select_start), str(select_end), str(final_performance)))
    print("Ratio x_axis: ", x_list)
    print("AUC axis: ", auc_list)
    print("Logloss axis: ", logloss_list)
    print("Selected data save to: ", selected_data_path)
    print("controller save to: ", save_controller_name)
    print("Best rs model save to: ", save_rs_name)


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--dataset_setting', default="amazon", help="'amazon' or 'ml-25m' for different tasks")
    parser.add_argument('--model_name', default='afm', help="Backbone model used. Currently only 'afm' is supported")
    parser.add_argument('--epoch', type=int, default=5, help="Training epoch of the backbone model")
    parser.add_argument('--ratio_num', type=int, default=20, help="The number of 'ratio' values generated \
    at equal intervals between 'select_start' and 'select_end'")
    parser.add_argument('--select_start', type=float, default=0.5)
    parser.add_argument('--select_end', type=float, default=0.95)
    parser.add_argument('--learning_rate', type=float, default=0.01, help="learning rate")
    parser.add_argument('--batch_size', type=int, default=2048, help="batch size")
    parser.add_argument('--weight_decay', type=float, default=1e-6, help="weight decay")
    parser.add_argument(
        '--device', default='CPU', help="device you are using. 'CPU' is available here")
    parser.add_argument('--save_dir', default='save_model', help="model saving path")
    parser.add_argument('--repeat_experiments', type=int, default=1, help="the \
    number of times you need to repeat the experiment. 1 is ok")
    parser.add_argument('--select_ratio', type=float, default=None, help="If \
    this is a float value in interval (0.0, 1.0), then the parameter 'ratio' will take this value, \
    otherwise it will search for the grid value according to the above 'ratio_num', 'select_start' and 'select_end'")
    parser.add_argument('--epsilon', type=float, default=0.1, help="A value to control randomized option in controller,"
                                                                   " usually a small value")
    args = parser.parse_args()

    for i in range(repeat_experiments):
        main(args.dataset_setting,
             args.model_name,
             args.epoch,
             args.ratio_num,
             args.select_start,
             args.select_end,
             args.learning_rate,
             args.batch_size,
             args.weight_decay,
             args.save_dir,
             args.repeat_experiments,
             args.select_ratio,
             args.epsilon)
