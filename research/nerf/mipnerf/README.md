# Contents

* [Contents](#contents)
    * [Mip-NeRF Description](#mip-nerf-description)
        * [Model Architecture](#model-architecture)
        * [Dataset](#dataset)
    * [Environment Requirements](#environment-requirements)
    * [Quick Start](#quick-start)
        * [Prepare the model](#prepare-the-model)
        * [Run the scripts](#run-the-scripts)
    * [Script Description](#script-description)
        * [Script and Sample Code](#script-and-sample-code)
        * [Script Parameters](#script-parameters)
    * [Training](#training)
        * [Training Process](#training-process)
    * [Evaluation](#evaluation)
        * [Evaluation Process](#evaluation-process)
            * [Evaluation on GPU](#evaluation-on-gpu)
        * [Evaluation result](#evaluation-result)
    * [Inference](#inference)
        * [Inference Process](#inference-process)
            * [Inference on GPU](#inference-on-gpu)
        * [Inference result](#inference-result)
    * [Video exporting](#video-exporting)
        * [Video exporting Process](#video-exporting-process)
            * [Video exporting on GPU](#video-exporting-on-gpu)
        * [Video exporting result](#video-exporting-result)
    * [3D mesh exporting](#3d-mesh-exporting)
        * [3D mesh exporting Process](#3d-mesh-exporting-process)
            * [3D mesh exporting on GPU](#3d-mesh-exporting-on-gpu)
        * [3D mesh exporting result](#3d-mesh-exporting-result)
    * [Model Description](#model-description)
         * [Performance](#performance)
    * [Description of Random Situation](#description-of-random-situation)
    * [ModelZoo Homepage](#modelzoo-homepage)

## [Mip-NeRF Description](#contents)

NeRF is a method that achieves state-of-the-art results for synthesizing novel
views of complex scenes by optimizing an underlying continuous volumetric scene function
using a sparse set of input views. The model synthesizes views by
querying 5D coordinates along camera rays and uses classic volume rendering techniques to project
the output colors and densities into an image. Because volume rendering is naturally differentiable,
the only input required to optimize our representation is a set of images with known camera poses.

Mip-NeRF extends NeRF to represent the scene at a continuously-valued
scale. By efficiently rendering anti-aliased conical frustums instead
of rays, Mip-NeRF reduces objectionable aliasing artifacts and
significantly improves NeRF's ability to represent fine details, while
also being 7% faster than NeRF and half the size.
Compared to NeRF, mip-NeRF reduces average error rates
by 17% on the dataset presented with NeRF and by 60% on a challenging
multiscale variant of that dataset that we present. mip-NeRF is also
able to match the accuracy of a brute-force supersampled NeRF on multiscale
dataset while being 22x faster.

Mip-NeRF paper:

[Paper](https://arxiv.org/abs/2103.13415): Jonathan T. Barron, Ben Mildenhall, Matthew Tancik, Peter Hedman, Ricardo Martin-Brualla, Pratul P. Srinivasan. CVPR, 2021.

### [Model Architecture](#contents)

NeRF represents a scene using a fully-connected (non-convolutional) deep network,
whose input is a single continuous 5D coordinate (spatial location (x,y,z) and viewing direction (θ,ϕ))
and whose output is the volume density and view-dependent emitted radiance at
that spatial location. Model encourages the representation to be multiview consistent by restricting
the network to predict the volume density as a function of only the location
x, while allowing the RGB color to be predicted as a function of both location
and viewing direction. To accomplish this, the multilayer perceptron first processes the input
3D coordinate x with 8 fully-connected layers (using ReLU activations and 256
channels per layer), and outputs the volume density and a 256-dimensional feature vector. This
feature vector is then concatenated with the camera ray’s viewing direction and
passed to one additional fully-connected layer (using a ReLU activation and 128
channels) that outputs the view-dependent RGB color.

### [Dataset](#contents)

Note that you can run the scripts based on the dataset mentioned in the original
paper or widely used in relevant domain/network architecture. In the following
sections, we will introduce how to run the scripts using the related dataset
below.

* Blender ("Realistic Synthetic 360"): pathtraced images of eight objects that exhibit
  complicated geometry and realistic non-Lambertian materials. Six are rendered from viewpoints sampled
  on the upper hemisphere, and two are rendered from viewpoints sampled on a full sphere. We render 100
  views of each scene as input and 200 for testing, all at 800 × 800 pixels.

* LLFF (“Real Forward-Facing”): real images of complex scenes captured with roughly forward-facing images.
  This dataset consists of 8 scenes captured with a handheld cellphone (5 taken from the LLFF paper and 3 that we capture),
  captured with 20 to 62 images, and hold out 1/8 of these for the test set. All images are 1008×756 pixels.

## [Environment Requirements](#contents)

* Download the datasets and locate to some folder `/path/to/the/dataset`:

    * [blender](https://drive.google.com/drive/folders/1JDdLGDruGNXWnM1eqY1FNL9PlStjaKWi)
    * [llff](https://drive.google.com/drive/folders/14boI-o5hGO9srnWaaogTU5_ji7wkX2S7)

* Install the requirements:

Use requirement.txt file with the following command:

```bash
pip install -r requirements.txt
```

## [Quick Start](#contents)

### [Prepare the model](#contents)

All necessary configs examples can be found in the project directory `src/configs`.

Training stage:

* dataset-based settings config (`*_ds_config.json`)
* dataset-based train config (`*_train_config.json`)
* nerf architecture config (`nerf_config.json`, the default in use)

Inference stage:

* dataset-based scene config (`inference/*_scene_config.json`)
* dataset-based poses config (`inference/*_poses.json`)
* nerf architecture config (`nerf_config.json`, the default in use)

Evaluation stage:

* dataset-based settings config (`*_ds_config.json`)
* nerf architecture config (the default in use)

Note: \* - dataset type (one from blender and llff).

1. Prepare the model directory: copy necessary configs for choosing dataset to some directory `/path/to/model_cfgs/` for future scripts launching.
2. Hyper parameters that recommended to be changed for training stage in dataset-based train config:

    * `train_rays_batch_size`
    * `val_rays_batch_size`
    * `epochs`
    * `val_epochs_freq`
    * `grad_clip`
    * `coarse_loss_mult`
    * `lr_delay_steps`
    * `lr_delay_mult`
    * `lr_init`
    * `lr_final`

3. Download corresponding checkpoints if need.

### [Run the scripts](#contents)

After preparing directory with configs `/path/to/model_cfgs/` you can start training and evaluation:

* running on GPU

```shell
# distributed training on GPU
bash scripts/run_distributed_training_gpu.sh [TRAIN_CONFIG] [DS_CONFIG] [DATASET_PATH] [OUT_DIR] [DEVICE_NUM] [PRETRAINED_CKPT_PATH](optional)

# standalone training on GPU
bash scripts/run_standalone_train_gpu.sh [TRAIN_CONFIG] [DS_CONFIG] [DATASET_PATH] [OUT_DIR] [PRETRAINED_CKPT_PATH](optional)

# run eval on GPU
bash scripts/run_eval_gpu.sh [DATA_PATH] [DATA_CONFIG] [CHECKPOINT_PATH] [OUT_PATH] [RAYS_BATCH]

# run inference on GPU
bash scripts/run_infer_gpu.sh [POSES] [SCENE_CONFIG] [CHECKPOINT_PATH] [OUT_PATH] [RAYS_BATCH]

# run export 3d mesh
bash scripts/run_export_3d.sh [SCENE_CONFIG] [CHECKPOINT_PATH] [OUT_STL]

# run export video
bash scripts/run_export_video.sh [POSES] [SCENE_CONFIG] [CHECKPOINT_PATH] [OUT_PATH] [RAYS_BATCH]
```

## [Script Description](#contents)

### [Script and Sample Code](#contents)

```shell
mipnerf
├── README.md                               ## Mip-NeRF description
├── eval.py                                 ## evaluation script
├── export_3d_mesh.py                       ## 3d model exporting
├── infer.py                                ## inference script
├── requirements.txt                        ## requirements
├── scripts
│   ├── run_distributed_training_gpu.sh     ## bash script for distributed training
│   ├── run_eval_gpu.sh                     ## bash script for evaluation
│   ├── run_export_3d.sh                    ## bash script for 3d mesh exporting
│   ├── run_export_video.sh                 ## bash script for video exporting
│   ├── run_infer_gpu.sh                    ## bash script for inference
│   └── run_standalone_train_gpu.sh         ## bash script for single gpu training
├── src
│   ├── configs
│   │   ├── blend_ds_config.json            ## blender dataset settings config
│   │   ├── blend_train_config.json         ## blender dataset train config
│   │   ├── inference
│   │   │   ├── blend_poses.json            ## blender dataset one pose config
│   │   │   ├── blend_scene_config.json     ## blender dataset scene config
│   │   │   ├── blend_video_poses.json      ## blender dataset poses for video exporting
│   │   │   ├── llff_poses.json             ## LLFF dataset one pose config
│   │   │   ├── llff_scene_config.json      ## LLFF dataset scene config
│   │   │   └── llff_video_poses.json       ## LLFF dataset poses for video exporting
│   │   ├── llff_ds_config.json             ## LLFF dataset settings config
│   │   ├── llff_train_config.json          ## LLFF dataset train config
│   │   └── nerf_config.json                ## Mip-NeRF architecture config
│   ├── data
│   │   ├── __init__.py
│   │   ├── blender_loader.py               ## blender dataset loader
│   │   ├── dataset.py                      ## dataset class
│   │   ├── llff_loader.py                  ## LLFF dataset loader
│   │   └── ray_utils.py                    ## rays utils
│   ├── tools
│   │   ├── __init__.py
│   │   ├── criterion.py                    ## loss function
│   │   └── metrics.py                      ## MSE and PSNR metrics
│   ├── train
│   │   ├── callbacks.py                    ## custom callbacks
│   │   └── mlflow_funcs.py                 ## mlflow auxiliary funcs
│   ├── trainer
│   │   ├── __init__.py
│   │   └── train_one_step.py               ## training wrapper for using grad_clip
│   └── volume_rendering
│       ├── __init__.py
│       ├── coordinates_samplers.py         ## Mip-NeRF coordinates sampling
│       ├── scene_representation.py         ## Mip-NeRF scene representation
│       └── volume_rendering.py             ## Mip-NeRF volume rendering pipeline
└── train.py                                ## training script
```

### [Script Parameters](#contents)

Train config parameters:

```bash
{
  "train_rays_batch_size": 4000,  # train rays batch size
  "val_rays_batch_size": 400,     # validation rays batch size (should be the divider of image height * width)
  "nerf_chunk": 30000,            # model chunk for points along rays processing

  "epochs": 64,                   # epochs number

  "val_epochs_freq": 1,           # epochs number to repeat the validation stage during training

  "optimizer": "adam",            # optimizer
  "grad_clip": 1.0,               # using gradient cliping (0.0 if False, 1.0 if True)

  "coarse_loss_mult": 0.1,        # coarse loss coefficient
  "lr_delay_steps": 2500,         # how often to reduce lr
  "lr_delay_mult": 0.01,          # how much to reduce lr
  "lr_init": 5.0e-4,              # initial learning rate
  "lr_final": 5.0e-6,             # final learning rate

  "perturbation": true,           # points along rays perturbation
  "raw_noise_std": 1.0,           # points along rays coordinates noise
}
```

Dataset settings config parameters differ based on the dataset. But the common
dataset settings:

```bash
{
  "data_type": "blender",             # dataset type - one from blender and llff
  "white_bkgd": true,                 # make white background after image loading - useful for the synthetic scenes
  "ndc_coord": false,                 # normal device coordinates space rays sampling
  "load_radii": true,
  "use_pixel_centers": true,
  "ray_shape": "cone",
  "epoch_size": 250000,
  "cam_scale_factor": 1,
  "density_noise": 1.0
}
```

## [Training](#contents)

To train the model, run `train.py`.

### [Training process](#contents)

Standalone training mode:

```bash
bash scripts/run_standalone_train_gpu.sh [TRAIN_CONFIG] [DS_CONFIG] [DATASET_PATH] [OUT_DIR] [PRETRAINED_CKPT_PATH](optional)
```

Bash script parameters:

* `TRAIN_CONFIG`: training loading settings config.
* `DS_CONFIG`: dataset scene loading settings config.
* `DATASET_PATH`: the path to the blender or llff dataset scene.
* `OUT_DIR`: output directory to store the training meta information.
* `PRETRAINED_CKPT_PATH`: the path of pretrained checkpoint file, it is better
 to use absolute path.

### [Distribute training](#contents)

Distribute training mode:

```bash
bash scripts/run_distributed_training_gpu.sh [TRAIN_CONFIG] [DS_CONFIG] [DATASET_PATH] [OUT_DIR] [DEVICE_NUM] [PRETRAINED_CKPT_PATH](optional)
```

Script parameters:

* `TRAIN_CONFIG`: training loading settings config.
* `DS_CONFIG`: dataset scene loading settings config.
* `DATASET_PATH`: the path to the blender or llff dataset scene.
* `OUT_DIR`: output directory to store the training meta information.
* `DEVICE_NUM`: devices count.
* `PRETRAINED_CKPT_PATH`: the path of pretrained checkpoint file, it is better
 to use absolute path.

Training result and checkpoints will be stored in the current path, whose folder name is "LOG".

## [Evaluation](#contents)

### [Evaluation process](#contents)

#### [Evaluation on GPU](#contents)

```bash
bash scripts/run_eval_gpu.sh [DATA_PATH] [DATA_CONFIG] [CHECKPOINT_PATH] [OUT_PATH] [RAYS_BATCH]
```

Script parameters:

* `DATA_PATH`: the path to the blender or llff dataset scene.
* `DATA_CONFIG`: dataset scene loading settings config.
* `CHECKPOINT_PATH`: the path of pretrained checkpoint file.
* `OUT_PATH`: output directory to store the evaluation result.
* `RAYS_BATCH`: rays batch number for NeRF evaluation. Should be the divider of image height * width.

### [Evaluation result](#contents)

Result:

* Store GT and predict images with the PSNR metric in the files name.
* Store the CSV file with image-wise PSNR value and the total PSNR.

## [Inference](#contents)

### [Inference process](#contents)

#### [Inference on GPU](#contents)

```bash
bash scripts/run_infer_gpu.sh [POSES] [SCENE_CONFIG] [CHECKPOINT_PATH] [OUT_PATH] [RAYS_BATCH]
```

Script parameters:

* `POSES`: poses for NeRF-based inference rendering.
* `SCENE_CONFIG`： scene config file. Can be found in the training output directory.
* `CHECKPOINT_PATH`: path to checkpoint.
* `OUT_PATH`: path to the output directory with NeRF based rendered images for poses from config.
* `RAYS_BATCH`: rays batch number for prediction.

### [Inference result](#contents)

Predictions are the rendered images for passed poses in pose config stored in the `OUT_PATH` directory.

## [Video exporting](#contents)

### [Video exporting process](#contents)

#### [Video exporting on GPU](#contents)

```bash
bash scripts/run_export_video.sh [POSES] [SCENE_CONFIG] [CHECKPOINT_PATH] [OUT_PATH] [RAYS_BATCH]
```

Script parameters:

* `POSES`: poses for NeRF-based inference rendering and video exporting.
* `SCENE_CONFIG`： scene config file. Can be found in the training output directory.
* `CHECKPOINT_PATH`: path to checkpoint.
* `OUT_PATH`: path to the output directory with NeRF based rendered images for poses from config and exported video.
* `RAYS_BATCH`: rays batch number for prediction.

### [Video exporting result](#contents)

Results are the rendered images for passed poses in pose config stored in the `OUT_PATH` directory
and the exported video is based on the rendered poses.

## [3D mesh exporting](#contents)

### [3D mesh exporting process](#contents)

#### [3D mesh exporting on GPU](#contents)

```bash
bash scripts/run_export_3d.sh [SCENE_CONFIG] [CHECKPOINT_PATH] [OUT_STL]
```

Script parameters:

* `SCENE_CONFIG`： scene config file. Can be found in the training output directory.
* `CHECKPOINT_PATH`: path to checkpoint.
* `OUT_STL`: output stl file for storing the 3d model.

### [3D mesh exporting result](#contents)

Result is the STL file with internal 3D mesh representation based on the coarse NeRF model raw output.
It is useful for synthetic scenes like blender dataset.

## [Model Description](#contents)

### [Performance](#contents)

| Parameters          | GPU                                                              |
|---------------------|------------------------------------------------------------------|
| Model Version       | Mip-NeRF                                                         |
| Resource            | 1xGPU(NVIDIA GeForce RTX 3090), CPU 2.1GHz 64 cores, Memory 256G |
| Uploaded Date       | 11/10/2023 (month/day/year)                                      |
| MindSpore Version   | 2.2.0                                                            |
| Dataset             | Blender (synthetic), LLFF (realistic)                            |
| Pretrained          | Noised checkpoint for blender lego (PSNR=34.93)                  |
| Training Parameters | batch_size = 4000, epochs number depends on the dataset          |
| Optimizer           | Adam                                                             |
| Loss Function       | MSE                                                              |
| Speed               | 4000 rays per batch: 1201.891 ms/step                            |
| Metric              | PSNR                                                             |
| PSNR                | Blender (lego) - 35.78, LLFF (fern) - 23.95                      |
| Configuration       | nerf_config.json                                                 |

## [Description of Random Situation](#contents)

There is no seed turned on.

## [ModelZoo Homepage](#contents)

Please check the official [homepage](https://gitee.com/mindspore/models).
