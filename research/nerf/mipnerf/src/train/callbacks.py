# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""Callbacks."""

import os
import datetime
import time
import logging
from pathlib import Path
from operator import lt, gt

import mindspore as ms
from mindspore._checkparam import check_positive_int
from mindspore import Callback, SummaryCollector, SummaryRecord, RunContext
from mindspore import (ModelCheckpoint, CheckpointConfig,
                       LossMonitor)

from .mlflow_funcs import mlflow_import, _get_rank


class BestCheckpointSavingCallback(Callback):
    """Callback to save best model checkpoints during training."""

    def __init__(
            self,
            ckpt_dir,
            target_metric='acc',
            best_is_max=True,
            prefix='',
            buffer=5
    ):
        """
        Initialize ckpt saving callback.

        Parameters
        ----------
        ckpt_dir: str
            Directory to save checkpoints to.
        target_metric: str
            Name of the metric listed in `metrics` parameter of Model.
        best_is_max: bool
            Flag to choose is the higher or lower metric value is better.
            For example:
                - if `target_metric=loss` then `best_is_max` should be False
                - if `target_metric=acc` then `best_is_max` should be True
        prefix: str
            Prefix of saved checkpoint file.
        buffer: int
            Max number of saved checkpoints.
        """
        self.ckpt_dir = Path(ckpt_dir)
        self._make_dir()
        self.target_metric = target_metric
        self.best_is_max = best_is_max
        self.prefix = prefix
        if best_is_max:
            self.best_metric = float('-inf')
            self.compare = lt
        else:
            self.best_metric = float('inf')
            self.compare = gt

        self.current_ckpt = []
        self.buffer_size = buffer

    def _make_dir(self):
        """Create a checkpoint directory."""
        if not self.ckpt_dir.exists():
            self.ckpt_dir.mkdir(parents=True)
            logging.info('Directory created: %s', self.ckpt_dir)
        else:
            logging.warning('Directory already exists: %s', self.ckpt_dir)

    def _save_checkpoint(self, network, epoch):
        """
        Save checkpoint.

        Parameters
        ----------
        network
            Network to save checkpoint for.
        """
        if not float('-inf') < self.best_metric < float('inf'):
            return
        ckpt_name = (f'epoch={epoch}_{self.target_metric}'
                     f'={self.best_metric:.3f}.ckpt')
        if self.prefix:
            ckpt_name = f'{self.prefix}_{ckpt_name}'
        ms.save_checkpoint(network, str(self.ckpt_dir / ckpt_name))

        self.current_ckpt.append(self.ckpt_dir / ckpt_name)
        if len(self.current_ckpt) > self.buffer_size:
            removed = self.current_ckpt[0]
            removed.unlink()
            del self.current_ckpt[0]

    def on_eval_end(self, run_context: RunContext):
        """
        Check and safe checkpoint if needed after evaluation complete.

        Parameters
        ----------
        run_context: RunContext

        """
        cb_params = run_context.original_args()
        metrics = {k: v for k, v in cb_params.eval_results.items() if v != 0}
        if self.target_metric not in metrics:
            raise KeyError(
                f'Target metric {self.target_metric} is not in '
                'cb_params.metrics.'
            )
        # If the new metric is better the previous "best"
        if self.compare(self.best_metric, metrics[self.target_metric]):
            self.best_metric = metrics[self.target_metric]
            self._save_checkpoint(
                cb_params.network, epoch=cb_params.cur_epoch_num
            )


class SummaryCallbackWithEval(SummaryCollector):
    """
    Callback that can collect a common information like SummaryCollector.

    Additionally, this callback collects:
        - learning rate
        - validation loss
        - validation accuracy
    """

    def __init__(
            self,
            summary_dir,
            log_path,
            collect_freq=10,
            collect_specified_data=None,
            keep_default_action=True,
            custom_lineage_data=None,
            collect_tensor_freq=None,
            max_file_size=None,
            export_options=None
    ):
        super().__init__(
            summary_dir=summary_dir,
            collect_freq=collect_freq,
            collect_specified_data=collect_specified_data,
            keep_default_action=keep_default_action,
            custom_lineage_data=custom_lineage_data,
            collect_tensor_freq=collect_tensor_freq,
            max_file_size=max_file_size,
            export_options=export_options
        )
        self.entered_count = 0
        self.log_path = log_path

    def on_train_epoch_end(self, run_context: RunContext):
        """
        Collect learning rate after train epoch.

        Parameters
        ----------
        run_context: RunContext
        """
        cb_params = run_context.original_args()
        optimizer = cb_params.get('optimizer')
        if optimizer is None:
            optimizer = getattr(cb_params.network, 'optimizer', None)
        if optimizer is None:
            logging.warning('There is no optimizer found!')
        else:
            global_step = optimizer.global_step
            lr = optimizer.learning_rate(global_step)
            self._record.add_value('scalar', 'Train/learning_rate',
                                   ms.Tensor(lr))
            self._record.record(cb_params.cur_epoch_num)
            super().on_train_epoch_end(run_context)

            mlflow = mlflow_import()
            if mlflow is not None and mlflow.active_run() is not None:
                mlflow.log_metric('Train/learning_rate', lr,
                                  cb_params.cur_epoch_num)

    def on_eval_end(self, run_context: RunContext):
        """
        Collect metrics after evaluation complete.

        Parameters
        ----------
        run_context: RunContext
        """
        cb_params = run_context.original_args()
        metrics = {k: v for k, v in cb_params.eval_results.items() if v != 0}
        final_msg = f'Result metrics for epoch {cb_params.cur_epoch_num}: ' \
                    + str({key: metrics[key] for key in sorted(metrics)})
        logging.debug(final_msg)

        mlflow = mlflow_import()
        for metric_name, value in metrics.items():
            self._record.add_value(
                'scalar', f'Metrics/{metric_name}', ms.Tensor(value)
            )
            if mlflow is not None and mlflow.active_run() is not None:
                mlflow.log_metric(f'Metrics/{metric_name}', value,
                                  cb_params.cur_epoch_num)

        self._record.record(cb_params.cur_epoch_num)
        self._record.flush()

        if mlflow is not None and mlflow.active_run() is not None:
            mlflow.log_artifact(self.log_path)

    def __enter__(self):
        """
        Enter in context manager and control that SummaryRecord created once.
        """
        if self.entered_count == 0:
            self._record = SummaryRecord(log_dir=self._summary_dir,
                                         max_file_size=self._max_file_size,
                                         raise_exception=False,
                                         export_options=self._export_options)
            self._first_step, self._dataset_sink_mode = True, True
        self.entered_count += 1
        return self

    def __exit__(self, *err):
        """
        Exit from context manager and control SummaryRecord correct closing.
        """
        self.entered_count -= 1
        if self.entered_count == 0:
            super().__exit__(err)


class TrainTimeMonitor(Callback):
    """
    Monitor the time in train process.

    Args:
        data_size (int): How many steps are the intervals between logging
            information each time.
            if the program get `batch_num` during training, `data_size`
            will be set to `batch_num`, otherwise `data_size` will be used.
            Default: None

    Raises:
        ValueError: If data_size is not positive int.
    """

    def __init__(self, data_size=None):
        super().__init__()
        self.data_size = data_size
        self.epoch_time = time.time()

    def on_train_epoch_begin(self, run_context):
        """
        Record time at the beginning of epoch.

        Args:
            run_context (RunContext): Context of the process running.
                For more details, please refer to :class:`mindspore.RunContext`
        """
        self.epoch_time = time.time()

    def on_train_epoch_end(self, run_context):
        """
        Log process cost time at the end of epoch.

        Args:
            run_context (RunContext): Context of the process running.
                For more details, please refer to :class:`mindspore.RunContext`
        """
        epoch_seconds = (time.time() - self.epoch_time) * 1000
        step_size = self.data_size
        cb_params = run_context.original_args()
        mode = cb_params.get("mode", "")
        if hasattr(cb_params, "batch_num"):
            batch_num = cb_params.batch_num
            if isinstance(batch_num, int) and batch_num > 0:
                step_size = cb_params.batch_num
        check_positive_int(step_size)

        step_seconds = epoch_seconds / step_size
        log_msg = f'{mode.title()} epoch time: {epoch_seconds:5.3f} ms, ' \
                  f'per step time: {step_seconds:5.3f} ms'
        logging.info(log_msg)


class EvalTimeMonitor(Callback):
    """
    Monitor the time in eval process.

    Args:
        data_size (int): How many steps are the intervals between logging
            information each time.
            if the program get `batch_num` during training, `data_size`
            will be set to `batch_num`, otherwise `data_size` will be used.
            Default: None

    Raises:
        ValueError: If data_size is not positive int.
    """

    def __init__(self, data_size=None):
        super().__init__()
        self.data_size = data_size
        self.epoch_time = time.time()

    def on_eval_epoch_begin(self, run_context):
        """
        Record time at the beginning of epoch.

        Args:
            run_context (RunContext): Context of the process running.
                For more details, please refer to :class:`mindspore.RunContext`
        """
        self.epoch_time = time.time()

    def on_eval_epoch_end(self, run_context):
        """
        Log process cost time at the end of epoch.

        Args:
            run_context (RunContext): Context of the process running.
                For more details, please refer to :class:`mindspore.RunContext`
        """
        epoch_seconds = (time.time() - self.epoch_time) * 1000
        step_size = self.data_size
        cb_params = run_context.original_args()
        mode = cb_params.get("mode", "")
        if hasattr(cb_params, "batch_num"):
            batch_num = cb_params.batch_num
            if isinstance(batch_num, int) and batch_num > 0:
                step_size = cb_params.batch_num
        check_positive_int(step_size)

        step_seconds = epoch_seconds / step_size
        log_msg = f'{mode.title()} epoch time: {epoch_seconds:5.3f} ms, ' \
                  f'per step time: {step_seconds:5.3f} ms'
        logging.info(log_msg)


def config_logging(
        filename_prefix, filemode='a', level=logging.INFO,
        log_format='[%(asctime)s.%(msecs)03d] %(levelname)s: %(message)s',
        datefmt='%Y-%m-%dT%H:%M:%S'
):
    """
    Configure logging.
    """
    rank = _get_rank()
    filename_suffix = '.log'
    if rank is not None:
        filename_suffix = f'_{rank}' + filename_suffix
    filename = filename_prefix + filename_suffix
    os.makedirs(os.path.dirname(filename), exist_ok=True)
    logging.basicConfig(level=level, datefmt=datefmt, format=log_format)
    file_handler = logging.FileHandler(filename, filemode)
    file_handler.setFormatter(logging.Formatter(log_format, datefmt))
    file_handler.setLevel(level)
    logging.getLogger().addHandler(file_handler)
    logging.getLogger().setLevel(level)


def get_callbacks(arch, rank, train_data_size, val_data_size, out_dir,
                  log_path='', ckpt_save_every_step=0, ckpt_save_every_sec=0,
                  ckpt_keep_num=10, print_loss_every=1,
                  collect_freq=0, collect_tensor_freq=None,
                  collect_graph=False, collect_input_data=False,
                  keep_default_action=False, logging_level=logging.INFO,
                  logging_format='%(levelname)s: %(message)s'):
    """Get common callbacks."""
    ckpt_dir = out_dir / 'ckpt'
    best_ckpt_dir = out_dir / 'best_ckpt_dir'
    summary_dir = out_dir / 'summary'
    cur_name = datetime.datetime.now().strftime('%y-%m-%d_%H%M%S')
    ckpt_save_dir = f'{ckpt_dir}/{cur_name}_{rank}'
    ckpt_best_save_dir = f'{best_ckpt_dir}/{cur_name}_{rank}'
    summary_dir = f'{summary_dir}/{cur_name}'

    logging.basicConfig(format=logging_format, level=logging_level)
    if collect_freq == 0:
        collect_freq = train_data_size
    if ckpt_save_every_step == 0 and ckpt_save_every_sec == 0:
        ckpt_save_every_step = train_data_size
    config_ck = CheckpointConfig(
        # To save every epoch use data.train_dataset.get_data_size(),
        save_checkpoint_steps=ckpt_save_every_step,
        save_checkpoint_seconds=ckpt_save_every_sec,
        keep_checkpoint_max=ckpt_keep_num,
        append_info=['epoch_num', 'step_num']
    )
    train_time_cb = TrainTimeMonitor(data_size=train_data_size)
    eval_time_cb = EvalTimeMonitor(data_size=val_data_size)

    best_ckpt_save_cb = BestCheckpointSavingCallback(
        ckpt_best_save_dir, prefix=arch, target_metric='PSNR'
    )

    ckpoint_cb = ModelCheckpoint(
        prefix=f'{arch}_{rank}',
        directory=ckpt_save_dir,
        config=config_ck
    )
    loss_cb = LossMonitor(print_loss_every)

    specified = {
        'collect_metric': True,
        'collect_train_lineage': True,
        'collect_eval_lineage': True,
        'collect_graph': collect_graph,
        'collect_input_data': collect_input_data,
    }
    summary_collector_cb = SummaryCallbackWithEval(
        summary_dir=summary_dir,
        log_path=log_path,
        collect_specified_data=specified,
        collect_freq=collect_freq,
        keep_default_action=keep_default_action,
        collect_tensor_freq=collect_tensor_freq
    )
    return [
        train_time_cb,
        eval_time_cb,
        ckpoint_cb,
        loss_cb,
        best_ckpt_save_cb,
        summary_collector_cb
    ]
