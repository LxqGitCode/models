# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
#
# This file or its part has been derived from the following repository
# and modified: https://github.com/mjmjeong/InfoNeRF
# ============================================================================
"""Loss functions for InfoNeRF model."""

import mindspore as ms
import mindspore.nn as nn

from mindspore.nn.loss.loss import LossBase


class NerfL2Loss(LossBase):

    def __init__(self):
        super().__init__()
        self.reduce_mean = ms.ops.ReduceMean()
        self.pow = ms.ops.Pow()

    def construct(self, out, target):
        loss_coarse = self.reduce_mean(self.pow((out[:, :3] - target), 2))
        loss_coarse = ms.ops.clip_by_value(
            loss_coarse,
            clip_value_min=ms.Tensor(0.0),
            clip_value_max=ms.Tensor(1000.0)
        )
        loss_fine = self.reduce_mean(self.pow((out[:, 3: 6] - target), 2))
        loss_fine = ms.ops.clip_by_value(
            loss_fine,
            clip_value_min=ms.Tensor(0.0),
            clip_value_max=ms.Tensor(1000.0)
        )
        return loss_coarse + loss_fine


class EntropyLoss(LossBase):
    def __init__(self, train_cfg):
        super(EntropyLoss, self).__init__()
        self.N_samples = train_cfg['N_rand']
        self.threshold = train_cfg["entropy_acc_threshold"]
        self.computing_entropy_all = train_cfg["computing_entropy_all"]
        self.smoothing = train_cfg["smoothing"]
        self.computing_ignore_smoothing = train_cfg["entropy_ignore_smoothing"]
        self.entropy_log_scaling = train_cfg["entropy_log_scaling"]
        self.N_entropy = train_cfg["N_entropy"]

        self.reduce_sum = ms.ops.ReduceSum()
        self.reduce_mean = ms.ops.ReduceMean()

        if train_cfg["entropy_type"] == 'log2':
            self.entropy = lambda prob: -1 * prob * ms.ops.log2(prob + 1e-10)
        elif train_cfg["entropy_type"] == '1-p':
            self.entropy = lambda prob: prob * ms.ops.log2(1 - prob)

        if self.N_entropy == 0:
            self.computing_entropy_all = True

    def construct(self, sigma, acc):
        if self.smoothing and self.computing_ignore_smoothing:
            N_smooth = sigma.size(0) // 2
            acc = acc[:N_smooth]
            sigma = sigma[:N_smooth]
        if not self.computing_entropy_all:
            acc = acc[self.N_samples:]
            sigma = sigma[self.N_samples:]
        ray_prob = sigma / (self.reduce_sum(sigma, -1).expand_dims(-1) + 1e-10)
        entropy_ray = self.entropy(ray_prob)
        entropy_ray_loss = self.reduce_sum(entropy_ray, -1)

        # masking no hitting position?
        mask = acc > self.threshold
        entropy_ray_loss *= mask
        if self.entropy_log_scaling:
            return ms.ops.log(self.reduce_mean(entropy_ray_loss) + 1e-10)
        return self.reduce_mean(entropy_ray_loss)


class SmoothingLoss(LossBase):
    def __init__(self, train_cfg):
        super(SmoothingLoss, self).__init__()

        self.criterion = ms.ops.KLDivLoss(reduction='none')

        if train_cfg["smoothing_activation"] == 'softmax':
            self.smoothing_activation = nn.Softmax(-1)
        elif train_cfg["smoothing_activation"] == 'norm':
            reduce_sum = ms.ops.ReduceSum(keep_dims=True)
            self.smoothing_activation = \
                lambda x: x / (reduce_sum(x, -1) + 1e-10) + 1e-10

    def construct(self, sigma):
        half_num = sigma.shape[0] // 2
        sigma_1 = sigma[:half_num]
        sigma_2 = sigma[half_num:]

        p = self.smoothing_activation(sigma_1)
        q = self.smoothing_activation(sigma_2)
        loss = self.criterion(p.log(), q)
        # calculate batch mean
        loss = loss.sum() / p.shape[0]
        return loss


class NetWithLoss(nn.Cell):
    """
    NetWithLoss: Wrapper for calculated loss of network.
    """

    def __init__(self, train_cfg, model):
        super(NetWithLoss, self).__init__()
        self.model = model
        self.criterion = NerfL2Loss()
        self.entropy_loss = EntropyLoss(train_cfg=train_cfg)
        self.smoothing_loss = SmoothingLoss(train_cfg=train_cfg)

        self.entropy_ray_lambda = train_cfg["entropy_ray_lambda"]
        self.smoothing_lambda = train_cfg["smoothing_lambda"]
        self.smoothing_rate = train_cfg["smoothing_rate"]
        self.smoothing_step_size = train_cfg["smoothing_step_size"]
        self.use_entropy_ray_loss = train_cfg["use_entropy_ray_loss"]
        self.use_smoothing_loss = train_cfg["use_smoothing_loss"]

    def construct(self, *inputs, **kwargs):
        data = inputs[0]
        label = inputs[1]
        predict, alpha, acc = self.model(data)
        loss = self.criterion(predict, label)
        if self.use_entropy_ray_loss:
            loss += self.entropy_ray_lambda * \
                    self.entropy_loss(alpha, acc)
        if self.use_smoothing_loss:
            smoothing_lambda = inputs[2]
            loss += smoothing_lambda * self.smoothing_loss(alpha)
        return loss


class WithEvalCell(nn.Cell):

    def __init__(self, train_cfg, network):
        super(WithEvalCell, self).__init__(auto_prefix=False)
        self.network = network
        self.criterion = NerfL2Loss()
        self.entropy_loss = EntropyLoss(train_cfg=train_cfg)
        self.smoothing_loss = SmoothingLoss(train_cfg=train_cfg)

        self.entropy_ray_lambda = train_cfg["entropy_ray_lambda"]
        self.smoothing_lambda = train_cfg["smoothing_lambda"]
        self.use_entropy_ray_loss = train_cfg["use_entropy_ray_loss"]
        self.use_smoothing_loss = train_cfg["use_smoothing_loss"]

    def construct(self, data, label):
        outputs, alpha, acc = self.network(data)
        loss = self.criterion(outputs, label)
        if self.use_entropy_ray_loss:
            loss += self.entropy_ray_lambda * \
                    self.entropy_loss(alpha, acc)
        if self.use_smoothing_loss:
            loss += self.smoothing_lambda * self.smoothing_loss(alpha)
        return loss, outputs, label
