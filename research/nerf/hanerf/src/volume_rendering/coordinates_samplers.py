# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
#
# This file or its part has been derived from the following repository
# and modified: https://github.com/yenchenlin/nerf-pytorch
# ============================================================================
"""MindSpore based coordinates sampling: depth and hierarchical samplers."""

import mindspore as ms
import mindspore.nn as nn
from mindspore import ops


class HierarchicalCoordsBuilder(nn.Cell):

    def __init__(self, hierarchical_samples: int,
                 perturbation: bool,
                 dtype=ms.float32):
        super().__init__()
        self.hierarchical_samples = hierarchical_samples
        self.perturbation = perturbation
        self.dtype = dtype
        self.linspace = ops.LinSpace()(ms.Tensor(0, dtype=self.dtype),
                                       ms.Tensor(1, dtype=self.dtype),
                                       self.hierarchical_samples)

        self.concat = ops.Concat(axis=-1)
        self.cumsum = ops.CumSum()
        self.gatherd = ops.GatherD()
        self.max = ops.Maximum()
        self.min = ops.Minimum()
        self.reduce_sum = ops.ReduceSum(keep_dims=True)
        self.stack = ops.Stack(axis=-1)
        self.tile = ops.Tile()
        self.zeros_like = ops.ZerosLike()
        self.search_sorted = ops.SearchSorted(right=True, dtype=ms.int32)
        self.ones_like = ops.OnesLike()

    def build_coords(self, bins, weights):
        weights = weights + 1e-5
        pdf = weights / self.reduce_sum(weights, -1)
        cdf = self.cumsum(pdf, -1)
        zeros_like = self.zeros_like(cdf[..., :1])
        cdf = self.concat((zeros_like, cdf))  # (batch, len(bins))

        if self.perturbation and self.training:
            u = ms.numpy.rand(list(cdf.shape[:-1]) +
                              [self.hierarchical_samples])
        else:
            u = self.tile(self.linspace, tuple(list(cdf.shape[:-1]) + [1]))
        inds = self.search_sorted(cdf, u)

        below = self.max(self.zeros_like(inds - 1), inds - 1)
        min_avlbl = (cdf.shape[-1] - 1) * self.ones_like(inds)
        above = self.min(min_avlbl, inds)

        inds_g = self.stack((below, above))  # (batch, fine_samples, 2)

        tiled_cdf = self.tile(cdf[:, None, :],
                              (1, self.hierarchical_samples, 1))
        cdf_g = self.gatherd(tiled_cdf, 2, inds_g)
        tiled_bins = self.tile(bins[:, None, :],
                               (1, self.hierarchical_samples, 1))
        bins_g = self.gatherd(tiled_bins, 2, inds_g)

        denom = (cdf_g[..., 1] - cdf_g[..., 0])
        denom = ms.numpy.where(denom < 1e-5, self.ones_like(denom), denom)
        t = (u - cdf_g[..., 0]) / denom
        samples = bins_g[..., 0] + t * (bins_g[..., 1] - bins_g[..., 0])
        return samples
