# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
#
# This file or its part has been derived from the following repository
# and modified: https://github.com/rover-xingyu/Ha-NeRF
# ============================================================================
"""Encode appearance and implicit mask networks."""

import mindspore.nn as nn


class EncodeAppearance(nn.Cell):
    def __init__(self, input_dim_a, output_nc=8):
        super(EncodeAppearance, self).__init__()
        dim = 64
        self.model = nn.SequentialCell(
            nn.ReflectionPad2d(3),
            nn.Conv2d(input_dim_a, dim, 7, 1, pad_mode="pad",
                      padding=0, has_bias=True),
            nn.ReLU(),  # size
            nn.ReflectionPad2d(1),
            nn.Conv2d(dim, dim * 2, 4, 2, pad_mode="pad",
                      padding=0, has_bias=True),
            nn.ReLU(),  # size/2
            nn.ReflectionPad2d(1),
            nn.Conv2d(dim * 2, dim * 4, 4, 2, pad_mode="pad",
                      padding=0, has_bias=True),
            nn.ReLU(),  # size/4
            nn.ReflectionPad2d(1),
            nn.Conv2d(dim * 4, dim * 4, 4, 2, pad_mode="pad",
                      padding=0, has_bias=True),
            nn.ReLU(),  # size/8
            nn.ReflectionPad2d(1),
            nn.Conv2d(dim * 4, dim * 4, 4, 2, pad_mode="pad",
                      padding=0, has_bias=True),
            nn.ReLU(),  # size/16
            nn.AdaptiveAvgPool2d(1),
            nn.Conv2d(dim * 4, output_nc, 1, 1, pad_mode="pad",
                      padding=0, has_bias=True))  # 1*1

    def construct(self, x):
        x = self.model(x)
        output = x.view((x.shape[0], -1))
        return output


class ImplicitMask(nn.Cell):
    def __init__(self, latent=128, W=256, in_channels_dir=42):
        super().__init__()
        self.mask_mapping = nn.SequentialCell(
            nn.Dense(latent + in_channels_dir, W), nn.ReLU(),
            nn.Dense(W, W), nn.ReLU(),
            nn.Dense(W, W), nn.ReLU(),
            nn.Dense(W, W), nn.ReLU(),
            nn.Dense(W, 1), nn.Sigmoid())

    def construct(self, x):
        mask = self.mask_mapping(x)
        return mask
