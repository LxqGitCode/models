# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
#
# This file or its part has been derived from the following repository
# and modified: https://github.com/rover-xingyu/Ha-NeRF
# ============================================================================
"""Loss functions for HA-NeRF model."""

import mindspore.nn as nn
import mindspore as ms

from mindspore import ops


class ExponentialAnnealingWeight(nn.Cell):
    def __init__(self, out_max, out_min, k):
        super().__init__()
        self.max = ms.Tensor(out_max, dtype=ms.float32)
        self.min = ms.Tensor(out_min, dtype=ms.float32)
        self.k = ms.Tensor(k, dtype=ms.float32)

    def construct(self, t_cur):
        output = max(self.min, self.max * ops.exp(-t_cur * self.k))
        return output


class HaNeRFLoss(nn.Cell):
    def __init__(self, hparams, coef=1):
        super().__init__()
        self.coef = ms.Tensor(coef, dtype=ms.float32)
        self.annealing = ExponentialAnnealingWeight(hparams["maskrs_max"],
                                                    hparams["maskrs_min"],
                                                    hparams["maskrs_k"])

    def mask_regularize(self, mask, size_delta, digit_delta):
        focus_epsilon = 0.02

        # l2 regularize
        loss_focus_size = ops.pow(mask, 2)
        loss_focus_size = ops.mean(loss_focus_size) * size_delta

        loss_focus_digit = 1 / (ops.pow((mask - 0.5), 2) + focus_epsilon)
        loss_focus_digit = ops.mean(loss_focus_digit) * digit_delta

        return loss_focus_size, loss_focus_digit

    def l2_regularize(self, mu):
        mu_2 = ops.pow(mu, 2)
        encoding_loss = ops.mean(mu_2)
        return encoding_loss

    def construct(self, inputs, targets, hparams, global_step):
        loss = ms.Tensor(0., dtype=ms.float32)

        if 'a_embedded' in inputs:
            loss += self.l2_regularize(inputs['a_embedded']) * \
                    hparams["weightKL"]
            if 'a_embedded_random_rec' in inputs:
                random_diff = \
                    ops.stop_gradient(inputs['a_embedded_random']) - \
                    inputs['a_embedded_random_rec']
                loss += \
                    ops.mean(ops.abs(random_diff)) * hparams["weightRecA"]
                rgb_diff = \
                    ops.stop_gradient(inputs['rgb_fine']) - \
                    inputs['rgb_fine_random']
                embedded_diff = \
                    ops.stop_gradient(inputs['a_embedded']) - \
                    ops.stop_gradient(inputs['a_embedded_random'])
                loss += hparams["weightMS"] * 1 / (
                    (ops.mean(ops.abs(rgb_diff)) /
                     ops.mean(ops.abs(embedded_diff))) + 1 * 1e-5
                )

        if 'out_mask' in inputs:
            loss += 0.5 * ((1 - ops.stop_gradient(inputs['out_mask'])) *
                           (inputs['rgb_coarse'] - targets) ** 2).mean()
        else:
            loss += 0.5 * ((inputs['rgb_coarse'] - targets) ** 2).mean()

        if 'rgb_fine' in inputs:
            if 'out_mask' in inputs:
                r_ms, r_md = \
                    self.mask_regularize(inputs['out_mask'],
                                         self.annealing(global_step),
                                         hparams["maskrd"])
                loss += r_ms
                loss += r_md
                f_l = 0.5 * ((1 - inputs['out_mask']) *
                             (inputs['rgb_fine'] - targets) ** 2
                             ).mean()
                loss += f_l
            else:
                loss += 0.5 * ((inputs['rgb_fine'] - targets) ** 2).mean()
        loss *= self.coef

        return loss
